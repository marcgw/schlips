#Schlips readme

This is a scheme interpreter written in C# for a lecture called "Design und Implementation fortgeschrittener Programmiersprachen" at Stuttgart Media University.  

#Documentation
Documentation of all functions can be found in file [documentation.md](schlips/src/master/documentation.md).

#Release
A compiled exe file is located in the folder _releases_.  
All files in this folder are mandatory for execution. The build was created for .Net 4.5 and the x86 architecture.