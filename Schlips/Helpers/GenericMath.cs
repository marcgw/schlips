﻿using Schlips.SchlipsTokenObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Schlips.Helpers
{

    class GenericMath
    {
        //source:
        // http://stackoverflow.com/questions/1749966/command-sharp-how-to-determine-whether-a-type-is-a-integerPart
        static HashSet<Type> NumericTypes = new HashSet<Type> {
            typeof(byte), typeof(sbyte), 
            typeof(UInt16), typeof(UInt32), typeof(UInt64),
            typeof(Int16), typeof(Int32), typeof(Int64),
            typeof(Decimal), typeof(Double), typeof(Single),
            typeof(BigInteger), typeof(Complex)
        };

        public static bool IsInteger(object obj)
        {
            var t = obj.GetType();
            return t!=typeof(Decimal) && t!=typeof(Double) && t!=typeof(Single);
        }
		public static bool IsNumeric(object obj)
		{
			return NumericTypes.Contains(obj.GetType());
		}

		public static bool IsNumeric(object obj, out Type type)
        {
			type = obj.GetType();
			return NumericTypes.Contains(type);
        }

		public static SchlipsNumber Add(SchlipsNumber n1, SchlipsNumber n2)
		{
			object result = 0;

            try{
                checked
                {
			    #region switches
			    switch (n1.NumberType)
			    {
				    case NumberTypes.Int16:
					    switch (n2.NumberType)
					    {
						    case NumberTypes.Int16:
							    result = (Int16)n1.Value + (Int16)n2.Value;
							    break;
						    case NumberTypes.Int32:
							    result = (Int16)n1.Value + (Int32)n2.Value;
							    break;
						    case NumberTypes.Int64:
							    result = (Int16)n1.Value + (Int64)n2.Value;
							    break;
						    case NumberTypes.BigInt:
							    result = (Int16)n1.Value + (System.Numerics.BigInteger)n2.Value;
							    break;
                            case NumberTypes.Float:
                            case NumberTypes.Double:
                                result = (Int16)n1.Value + (double)n2.Value;
                                break;
						    default:
							    throw new ArgumentException("cannot handle: Int16 + '" + n2.Value.GetType()+"'" );
					    }
					    break;

				    case NumberTypes.Int32:
					    switch (n2.NumberType)
					    {
						    case NumberTypes.Int16:
							    result = (Int32)n1.Value + (Int16)n2.Value;
							    break;
						    case NumberTypes.Int32:
							    result = (Int32)n1.Value + (Int32)n2.Value;
							    break;
						    case NumberTypes.Int64:
							    result = (Int32)n1.Value + (Int64)n2.Value;
							    break;
						    case NumberTypes.BigInt:
							    result = (Int32)n1.Value + (System.Numerics.BigInteger)n2.Value;
							    break;
                            case NumberTypes.Float:
                            case NumberTypes.Double:
                                result = (Int32)n1.Value + (double)n2.Value;
                                break;
						    default:
							    throw new ArgumentException("cannot handle: Int32 + '" + n2.Value.GetType()+"'" );
					    }
					    break;
				
				    case NumberTypes.Int64:
					    switch (n2.NumberType)
					    {
						    case NumberTypes.Int16:
							    result = (Int64)n1.Value + (Int16)n2.Value;
							    break;
						    case NumberTypes.Int32:
							    result = (Int64)n1.Value + (Int32)n2.Value;
							    break;
						    case NumberTypes.Int64:
							    result = (Int64)n1.Value + (Int64)n2.Value;
							    break;
						    case NumberTypes.BigInt:
							    result = (Int64)n1.Value + (BigInteger)n2.Value;
							    break;
                            case NumberTypes.Float:
                            case NumberTypes.Double:
                                result = (Int64)n1.Value + (double)n2.Value;
                                break;
						    default:
							    throw new ArgumentException("cannot handle: Int64 + '" + n2.Value.GetType()+"'" );
					    }
					    break;
                    case NumberTypes.Float:
                    case NumberTypes.Double:
                        result = (double)n1.Value + (double)n2.Value;
                        break;
				    default:
					    throw new ArgumentException("cannot handle: '"+ n1.Value.GetType() + " + " + n2.Value.GetType()+"'" );
			    }
			    #endregion
                }
            }
            catch (OverflowException)
            {
                throw new NotImplementedException("GenericMath.Add() number OverflowException");
            }

            var resultObj = new SchlipsNumber(result);
			return resultObj;
            //return new SchlipsNumber(result);
		}

        //todo: unused
        public static object Multiply(object numberA, object numberB)
        {
            var result = numberA;
            Type numberClass = numberA.GetType();

            try
            {
                result = checked(MiscUtil.Operator.Multiply(numberA, numberB));
            }
            catch (OverflowException)
            {
                if (numberClass == typeof(Int16))
                {
                    numberA = (Int32)numberA;
                    numberClass = typeof(Int32);
                }
                else if (numberClass == typeof(Int32))
                {
                    numberA = (Int64)numberA;
                    numberClass = typeof(Int64);
                }

                return Multiply(numberA, numberB);
            }
            throw new NotImplementedException();
        }

        internal static BigInteger Add(object numberA, object NumberB)
        {
            BigInteger result = (BigInteger)MiscUtil.Operator.Add(numberA, NumberB);

            return result;
        }
    }
}
