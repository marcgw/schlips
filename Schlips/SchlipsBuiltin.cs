﻿using Schlips.SchlipsTokenObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips
{
    public enum SchlipsBuiltin
    {
        Nil, True, False,

        Quote, Define, Lambda,
        Cons, Car, Cdr,

        Add, Subtract, Multiply, Divide,

        If, And, Or, Not,
        NumberEqual, GreaterThan, GreaterEqual, SmallerThan, SmallerEqual
    }

    public class Singletons
    {
        private static SchlipsAtom _true = null;
        public static SchlipsAtom True()
        {
            if (_true == null) _true = new SchlipsAtom(SchlipsBuiltin.True);
            return _true;
        }

        private static SchlipsAtom _false = null;
        public static SchlipsAtom False()
        {
            if (_false == null) _false = new SchlipsAtom(SchlipsBuiltin.False);
            return _false;
        }

        private static SchlipsAtom _nil = null;
        internal static SchlipsTokenObject Nil()
        {
            if (_nil == null) _nil = new SchlipsAtom(SchlipsBuiltin.Nil);
            return _nil;
        }
    }


    public class SchlipsProcedure : SchlipsTokenObject
    {
        public SchlipsBuiltin Type { get; private set; }
        public int ExpectedOperands { get; protected set; }
        public List<SchlipsTokenObject> Operands { get; protected set; }

        public SchlipsProcedure(SchlipsBuiltin type)
        {
            Type = type;
            Operands = null;            
        }
        public SchlipsProcedure SetExpectedOperandCount(int expectedOperands)
        {
            ExpectedOperands = expectedOperands;
            int initialSize = (expectedOperands > 0) ? expectedOperands : 2;
            Operands = new List<SchlipsTokenObject>(initialSize);            
            return this;
        }

        public override string ToString()
        {
            return "<Builtin:" + Type.ToString("G") + ">";
        }

        #region typecasting
        public override bool IsFunction { get { return true; } }
        public override bool IsExecutable
        {
            get
            {
                int count = (Operands == null) ? 0 : Operands.Count;
                return (ExpectedOperands < 0 && count > 0) //these are functions that support an arbitrary integerPart of operands (Add, ..)
                    || ExpectedOperands == count;
            }
        }
        public bool Is(SchlipsBuiltin type) { return type == Type; }
        #endregion


        public override SchlipsTokenObject Retrieve()
        {
            var result = new SchlipsProcedure(Type);

            if (IsExecutable)
            {// copy reference to operands if operands are set
                result.ExpectedOperands = ExpectedOperands;
                result.Operands = Operands;
            }
            else 
            {// create a new operand list
                result.SetExpectedOperandCount(ExpectedOperands);
            }
            return result;    
        }



        internal virtual void AppendOperand(SchlipsTokenObject operand)
        {
            if (ExpectedOperands >= 0 && Operands.Count >= ExpectedOperands)
            {
                throw new SchlipsArgumentException(String.Format(
                    "{0} only expects {1} operands, but operand integerPart {2} was passed", this, ExpectedOperands, ExpectedOperands+1
                ));
            }

            Operands.Add(operand);
        }
    }


    public class SchlipsUDF : SchlipsProcedure
    {
        public SchlipsTokenObject Body { get; internal set; }
        public List<SchlipsSymbol> Formals { get; internal set; }

        public SchlipsUDF() : base(SchlipsBuiltin.Lambda)
        {
            ExpectedOperands = -1;
            Formals = null;
            Body = null;
        }
        
        //if IsDefined returns true, this UserDefinedFunction is callable
        public bool IsDefined
        {
            get
            {
                if (Formals == null || ExpectedOperands == -1 || Body == null) { return false; }
                return (ExpectedOperands == Formals.Count);
            }
        }

        internal override void AppendOperand(SchlipsTokenObject operand)
        {
            if (IsDefined)
            {// the passed Operands are used to call the function

                if (IsExecutable)
                {//all operands for this functions are set -> these operands are meant for a UDF in the body
                    var innerFunction = Body as SchlipsUDF;
                    if (innerFunction == null) throw new SchlipsArgumentException(
                        "Arity mismatch: All operands of this Lambda are already set. Don't try to set them again."
                    );
                    innerFunction.AppendOperand(operand);
                }
                else
                {
                    base.AppendOperand(operand);
                }
            }
            else
            {// the passed Operands are used to define the function
                //first Operand -> Formals (needs to be a Node)
                if (Formals == null)
                {
                    var node = operand as SchlipsNode;
                    if (node == null) throw new SchlipsArgumentException(String.Format(
                        "First operand passed to a Lambda Function needs to be of type SchlipsNode, but type {0} was passed",
                        operand.GetType().ToString()
                    ));

                    Formals = new List<SchlipsSymbol>();
                    
                    foreach (var formal in node)
                    {
                        if (!formal.IsSymbol) throw new SchlipsArgumentException(String.Format(
                            "Only SchemeSymbols may be used as formals for a Lambda, but type {0} was given.",
                            formal.GetType().ToString()
                        ));
                        
                        Formals.Add((SchlipsSymbol)formal);
                    }

                    SetExpectedOperandCount(Formals.Count);
                }
                // second operand -> function body
                else if (Body == null)
                {
                    Body = operand;
                }
                // third operand -> is not allowed
                else
                {
                    throw new SchlipsArgumentException("Arity mismatch: Lambda expects 2 operands");
                }
            }
        }

        public override bool IsExecutable
        {
            get
            {
                if (IsDefined)
                {
                    //this one works if UDF.IsDefined returns true
                    return base.IsExecutable;
                }
                return false;
            }
        }

        public override SchlipsTokenObject Retrieve()
        {
            var result = new SchlipsUDF();
            if (IsDefined)
            {
                result.SetExpectedOperandCount(ExpectedOperands);

                if (IsExecutable)
                {//copy set operands
                    result.ExpectedOperands = ExpectedOperands;
                    foreach (var operand in Operands)
                    {
                        //Procedures and UserDefinedFunctions need to be copied -> Retrieve method
                        result.Operands.Add(operand.Retrieve());
                    }
                }
                
                //copy Body (mutable state)
                result.Body = Body.Retrieve();
                //copy reference to Formals (immutable)
                result.Formals = Formals;
            }

            return result;
        }

        //for debugging purposes
        public string Bindings
        {
            get
            {
                string result = "(";
                SchlipsTokenObject obj;
                if (Operands != null)
                {
                    for (int i=0; i<Formals.Count; i++)
                    {
                        obj = Operands[i] ?? null;
                        result += Formals[i] + "=" + obj.ToString() + ",";
                    }
                }
                return result + ")";
            }
        }
    }



}
