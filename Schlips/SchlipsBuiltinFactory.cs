﻿using System;
using Schlips.SchlipsTokenObjects;


namespace Schlips
{
    class SchlipsBuiltinFactory
    {
        internal static SchlipsProcedure Create(SchlipsBuiltin type)
        {
            SchlipsProcedure result;

            if (type == SchlipsBuiltin.Lambda)
            {
                result = new SchlipsUDF();
            }
            else
            {
                result = new SchlipsProcedure(type);

                //set integerPart of Arguments
                switch (type)
                {
                    case SchlipsBuiltin.Quote:
                    case SchlipsBuiltin.Car:
                    case SchlipsBuiltin.Cdr:
                    case SchlipsBuiltin.Not:
                        result.SetExpectedOperandCount(1);
                        break;
                    case SchlipsBuiltin.Define:
                    case SchlipsBuiltin.Cons:
                    case SchlipsBuiltin.NumberEqual:
                    case SchlipsBuiltin.GreaterThan:
                    case SchlipsBuiltin.SmallerThan:
                    case SchlipsBuiltin.GreaterEqual:
                    case SchlipsBuiltin.SmallerEqual:
                        result.SetExpectedOperandCount(2);
                        break;
                    case SchlipsBuiltin.Add:
                    case SchlipsBuiltin.Subtract:
                    case SchlipsBuiltin.Multiply:
                    case SchlipsBuiltin.Divide:
                    case SchlipsBuiltin.And:
                    case SchlipsBuiltin.Or:
                        result.SetExpectedOperandCount(-1);
                        break;
                    case SchlipsBuiltin.If:
                        result.SetExpectedOperandCount(3);
                        break;
                    default:
                        throw new SchlipsArgumentException("Cannot create a Function Object of type '" + type.ToString("G") + "'");
                }
            }

            return result;
        }
    }
}
