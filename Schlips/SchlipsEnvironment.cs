﻿using Schlips.SchlipsTokenObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schlips
{
    public class SchlipsEnvironment
    {
        private SchlipsEnvironment _parentEnvironment = null;
        public Dictionary<SchlipsSymbol, SchlipsTokenObject> bindings;

        public SchlipsEnvironment(): this(null) { }

        public SchlipsEnvironment(SchlipsEnvironment parentEnvironment)
        {
            this._parentEnvironment = parentEnvironment;
            bindings = new Dictionary<SchlipsSymbol, SchlipsTokenObject>();
        }


        public bool TryGetBinding(SchlipsSymbol key, out SchlipsTokenObject result)
        {
            var foundBinding = bindings.TryGetValue(key, out result);

            if (!foundBinding && HasParentEnvironment)
            {
                foundBinding = ParentEnvironment.TryGetBinding(key, out result);
            }

            return foundBinding;
        }

        public SchlipsTokenObject GetBindingOrNull(SchlipsSymbol key)
        {
            SchlipsTokenObject result;
            if (!TryGetBinding(key, out result))
            {
                result = null;
            }

            return result;
        }

        public SchlipsTokenObject GetBindingOrSymbol(SchlipsSymbol key)
        {
            SchlipsTokenObject result;
            if (TryGetBinding(key, out result))
            {
                return result.Retrieve();
            }
            else 
            {
                result = key;
            }

            return result;
        }

        public void SetBinding(SchlipsSymbol key, SchlipsTokenObject value)
        {
            if (bindings.ContainsKey(key))
            {
                bindings[key] = value;
            }
            else
            {
                bindings.Add(key, value);
            }
        }


        public SchlipsEnvironment SpawnChildEnvironment()
        {
            return new SchlipsEnvironment(this);
        }


        public bool HasParentEnvironment { get { return _parentEnvironment != null; } }

        public SchlipsEnvironment ParentEnvironment
        {
            get
            {
                if (!HasParentEnvironment)
                {
                    throw new ArgumentException("This SchlipsEnvironment does not have a ParentEnvironment");
                }
                return _parentEnvironment;
            }
        }

    }



    public class SchlipsEnvironmentFactory
    {
		private static SchlipsEnvironment _rootEnvironment = null;
        
        public static SchlipsEnvironment RootEnvironment
        {
            get
            {
                if (_rootEnvironment == null) { GenerateRootEnvironment(); }
                return _rootEnvironment;
            }
        }

        public static void GenerateRootEnvironment()
        {
            _rootEnvironment = new SchlipsEnvironment();
            
            _rootEnvironment.SetBinding(new SchlipsSymbol("#t"), Singletons.True());
            _rootEnvironment.SetBinding(new SchlipsSymbol("#f"), Singletons.False());
            _rootEnvironment.SetBinding(new SchlipsSymbol("nil"), Singletons.Nil());
            
            _rootEnvironment.SetBinding(new SchlipsSymbol("quote"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Quote));
            _rootEnvironment.SetBinding(new SchlipsSymbol("+"), 
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Add));
            _rootEnvironment.SetBinding(new SchlipsSymbol("-"), 
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Subtract));
            _rootEnvironment.SetBinding(new SchlipsSymbol("*"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Multiply));
            _rootEnvironment.SetBinding(new SchlipsSymbol("/"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Divide));
            _rootEnvironment.SetBinding(new SchlipsSymbol("define"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Define));
            _rootEnvironment.SetBinding(new SchlipsSymbol("lambda"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Lambda));
            _rootEnvironment.SetBinding(new SchlipsSymbol("cons"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Cons));
            _rootEnvironment.SetBinding(new SchlipsSymbol("car"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Car));
            _rootEnvironment.SetBinding(new SchlipsSymbol("cdr"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Cdr));
            _rootEnvironment.SetBinding(new SchlipsSymbol("if"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.If));
            _rootEnvironment.SetBinding(new SchlipsSymbol("not"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Not));
            _rootEnvironment.SetBinding(new SchlipsSymbol("and"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.And));
            _rootEnvironment.SetBinding(new SchlipsSymbol("or"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.Or));
            _rootEnvironment.SetBinding(new SchlipsSymbol("="),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.NumberEqual));
            _rootEnvironment.SetBinding(new SchlipsSymbol(">"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.GreaterThan));
            _rootEnvironment.SetBinding(new SchlipsSymbol(">="),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.GreaterEqual));
            _rootEnvironment.SetBinding(new SchlipsSymbol("<"),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.SmallerThan));
            _rootEnvironment.SetBinding(new SchlipsSymbol("<="),
                SchlipsBuiltinFactory.Create(SchlipsBuiltin.SmallerEqual));
        }
    }
}
