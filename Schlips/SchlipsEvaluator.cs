﻿using Schlips.SchlipsTokenObjects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips
{
    public class SchlipsEvaluator
    {
        public SchlipsEnvironment Environment { get; private set; }

        private SchlipsParser AttachedParser = null;

        public SchlipsEvaluator(SchlipsParser parser, SchlipsEnvironment environment = null)
        {
            AttachedParser = parser;
            if (environment == null) { environment = SchlipsEnvironmentFactory.RootEnvironment; }
            Environment = environment;
        }

        public SchlipsTokenObject Evaluate(SchlipsTokenObject input)
        {
            SchlipsTokenObject result;
            //TODO: IsAtomic (Number, True, False)
            if (input.IsAtomic || input.IsNil)
            {
                result = input;
            }
            else if (input.IsSymbol)
            {
                result = Environment.GetBindingOrSymbol((SchlipsSymbol)input);
            }
            else if (input.IsFunction)
            {
                if (input.IsExecutable)
                {
                    result = EvaluateFunction((SchlipsProcedure)input);
                }
                else
                {
                    result = input;
                }
            }
            else
            {
                throw new NotImplementedException(String.Format(
                    "Evaluate({0}) is not implemented - type: {1}", input.ToString(), input.GetType().ToString()
                ));
            }

            return result;
        }
        
        private SchlipsTokenObject EvaluateFunction(SchlipsProcedure fn)
        {//only functions are passed that return IsExecutable==true
            SchlipsTokenObject evaluatedProcedure;

            if (TryToEvaluateMathFunctions(fn, out evaluatedProcedure))
            {
                return evaluatedProcedure;
            }
            else if (TryToEvaluateListProcessingFunctions(fn, out evaluatedProcedure))
            {
                return evaluatedProcedure;
            }
            else if (TryToEvaluateConditionals(fn, out evaluatedProcedure))
            {
                return evaluatedProcedure;
            }
            else if (fn.Is(SchlipsBuiltin.Quote))
            {
                return fn.Operands[0];
            }

            #region define
            else if (fn.Is(SchlipsBuiltin.Define))
            {
                var identifier = fn.Operands[0];

                if (identifier.IsSymbol)
                {
                    var expression = fn.Operands[1];
                    if (expression.IsFunction)
                    {//don't try to evaluate functions, they are stored directly
                    }
                    else
                    {
                        expression = Evaluate(expression);
                    }

                    Environment.SetBinding((SchlipsSymbol)identifier, expression);
                }
                else
                {
                    throw new SchlipsArgumentException("identifier should return a SchlipsSymbol, but was of type '" + identifier.GetType().ToString() + "'");
                }
                return Singletons.Nil();
            }
            #endregion

            #region lambda
            else if (fn.Is(SchlipsBuiltin.Lambda))
            {//It was checked before that fn.IsDefined == fn.IsExecutable == true
                var lambda = fn as SchlipsUDF;
                SchlipsTokenObject result = Singletons.Nil();

                if (lambda.Formals.Count > 0)
                {
                    PushEnvironment();
                    SchlipsTokenObject evaluatedOperand;

                    for (int i = 0; i < lambda.Formals.Count; i++)
                    {
                        evaluatedOperand = Evaluate(lambda.Operands[i]);
                        Environment.SetBinding(lambda.Formals[i], evaluatedOperand);
                    }

                    result = Evaluate(lambda.Body);

                    PopEnvironment();
                }
                else
                {
                    result = Evaluate(lambda.Body);
                }

                return result;
            }
            #endregion


            else
            {
                throw new NotImplementedException(String.Format(
                    "EvaluateFunction({0}) is not implemented", fn.GetType().ToString()
                ));
            }

        }

        #region Conses
        private bool TryToEvaluateListProcessingFunctions(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            evaluatedObject = null;
            SchlipsTokenObject obj;

            if (fn.Is(SchlipsBuiltin.Cons))
            {
                var op1 = Evaluate(fn.Operands[0]);
                var op2 = Evaluate(fn.Operands[1]);
                evaluatedObject = new SchlipsNode(op1, op2);
            }
            else if (fn.Is(SchlipsBuiltin.Car))
            {
                obj = Evaluate(fn.Operands[0]);
                var node = obj as SchlipsNode;
                if (node == null) throw new SchlipsArgumentException("car only works on SchlipsNodes");
                evaluatedObject = node.Head;
            }
            else if (fn.Is(SchlipsBuiltin.Cdr))
            {
                obj = Evaluate(fn.Operands[0]);
                var node = obj as SchlipsNode;
                if (node == null) throw new SchlipsArgumentException("cdr only works on SchlipsNodes");
                evaluatedObject = node.Tail;
            }

            return evaluatedObject != null;
        }
        #endregion

        #region math functions
        //Try to evaluate the procedure as a math function and return true on success
        private bool TryToEvaluateMathFunctions(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            evaluatedObject = null;

            if (fn.Is(SchlipsBuiltin.Add))
            {
                EvaluateAddFunction(fn, out evaluatedObject);
            }
            else if (fn.Is(SchlipsBuiltin.Subtract))
            {
                EvaluateSubtractFunction(fn, out evaluatedObject);
            }
            else if (fn.Is(SchlipsBuiltin.Multiply))
            {
                EvaluateMultiplyFunction(fn, out evaluatedObject);
            }
            else if (fn.Is(SchlipsBuiltin.Divide))
            {
                EvaluateDivideFunction(fn, out evaluatedObject);
            }

            return evaluatedObject != null;
        }

        private void EvaluateAddFunction(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            SchlipsNumber result = new SchlipsNumber(0);
            SchlipsNumber number = null;
            SchlipsTokenObject evaluated;

            if (fn.Operands != null)
            {
                foreach (var possibleNumber in fn.Operands)
                {
                    evaluated = Evaluate(possibleNumber);
                    number = evaluated as SchlipsNumber;
                    ThrowExceptionIfNotANumber(fn, number, evaluated, possibleNumber);
                    result += number;
                }
            }
            evaluatedObject = result;
        }

        private void EvaluateSubtractFunction(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            SchlipsNumber result = new SchlipsNumber(0);
            SchlipsNumber number = null;
            SchlipsTokenObject evaluated;

            if (fn.Operands == null)
            {
                throw new SchlipsArgumentException(fn.ToString() + " expects at least one operand.");
            }

            var first = true;
            foreach (var possibleNumber in fn.Operands)
            {
                evaluated = Evaluate(possibleNumber);
                number = evaluated as SchlipsNumber;
                ThrowExceptionIfNotANumber(fn, number, evaluated, possibleNumber);

                if (first && fn.Operands.Count > 1)
                {
                    result = number;
                    first = false;
                }
                else
                {
                    result -= number;
                }
            }

            evaluatedObject = result;
        }

        private void EvaluateMultiplyFunction(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            SchlipsNumber result = new SchlipsNumber(1);
            SchlipsNumber number = null;
            SchlipsTokenObject evaluated;

            if (fn.Operands != null)
            {
                foreach (var possibleNumber in fn.Operands)
                {
                    evaluated = (possibleNumber.IsNumber) ? possibleNumber : Evaluate(possibleNumber);
                    number = evaluated as SchlipsNumber;
                    ThrowExceptionIfNotANumber(fn, number, evaluated, possibleNumber);
                    result *= number;
                }
            }
            evaluatedObject = result;
        }

        private void EvaluateDivideFunction(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            SchlipsNumber result = new SchlipsNumber(0);
            SchlipsNumber number = null;
            SchlipsTokenObject evaluated;

            if (fn.Operands == null)
            {
                throw new SchlipsArgumentException(fn.ToString() + " expects at least one operand.");
            }

            var first = true;
            foreach (var possibleNumber in fn.Operands)
            {
                evaluated = Evaluate(possibleNumber);
                number = evaluated as SchlipsNumber;
                ThrowExceptionIfNotANumber(fn, number, evaluated, possibleNumber);

                if (first && fn.Operands.Count > 1)
                {
                    result = number;
                    first = false;
                }
                else if (((double)number.Value) == 0)
                {
                    throw new SchlipsArgumentException(fn.ToString() + " cannot divide by zero");
                }
                else
                {
                    result /= number;
                }
            }

            evaluatedObject = result;
        }

        private static void ThrowExceptionIfNotANumber(SchlipsProcedure fn, SchlipsNumber number, SchlipsTokenObject evaluated, SchlipsTokenObject possibleNumber)
        {
            if (number == null)
            {
                throw new SchlipsArgumentException(fn.ToString() + " can only handle SchlipsNumbers, but '"
                    + possibleNumber.ToString() + "' evaluated to '" + evaluated.ToString() + "'");
            }
        }
        #endregion

        #region conditionals
        private bool TryToEvaluateConditionals(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            evaluatedObject = null;
            SchlipsTokenObject obj = new SchlipsTokenObject();

            switch (fn.Type)
            {
                case SchlipsBuiltin.If:
                    obj = Evaluate(fn.Operands[0]);
                    var toEvaluate = (obj.IsTrue ? fn.Operands[1] : fn.Operands[2]);
                    var parsedObject = AttachedParser.Parse(toEvaluate);
                    evaluatedObject = Evaluate(parsedObject);
                    break;

                case SchlipsBuiltin.And:
                    foreach (var op in fn.Operands)
                    {
                        obj = Evaluate(op);
                        if (obj.IsFalse) break;
                    }
                    evaluatedObject = obj.IsTrue ? Singletons.True() : Singletons.False();
                    break;

                case SchlipsBuiltin.Or:
                    foreach (var op in fn.Operands)
                    {
                        obj = Evaluate(op);
                        if (obj.IsTrue) break;
                    }
                    evaluatedObject = obj.IsTrue ? Singletons.True() : Singletons.False();
                    break;

                case SchlipsBuiltin.Not:
                    obj = Evaluate(fn.Operands[0]);
                    evaluatedObject = (obj.IsTrue ? Singletons.False() : Singletons.True());
                    break;

                case SchlipsBuiltin.NumberEqual:
                case SchlipsBuiltin.GreaterThan:
                case SchlipsBuiltin.GreaterEqual:
                case SchlipsBuiltin.SmallerThan:
                case SchlipsBuiltin.SmallerEqual:
                    TryToEvaluateNumberConditionals(fn, out evaluatedObject);
                    break;
                default:
                    break;
            }

            return evaluatedObject != null;
        }

        private bool TryToEvaluateNumberConditionals(SchlipsProcedure fn, out SchlipsTokenObject evaluatedObject)
        {
            evaluatedObject = null;

            if (fn.Type >= SchlipsBuiltin.NumberEqual && fn.Type <= SchlipsBuiltin.SmallerEqual)
            {
                var obj1 = Evaluate(fn.Operands[0]);
                var obj2 = Evaluate(fn.Operands[1]);
                var result = false;
                
                var exceptionMsg = fn.ToString() + " only compares SchlipsNumbers, but '{0}' evaluated to a {1} with value '{2}'.";

                if (!obj1.IsNumber) throw new SchlipsArgumentException(String.Format(exceptionMsg,
                    fn.Operands[0], obj1.GetType().ToString(), obj1));

                if (!obj2.IsNumber) throw new SchlipsArgumentException(String.Format(exceptionMsg,
                    fn.Operands[1], obj2.GetType().ToString(), obj2));

                var nr1 = obj1 as SchlipsNumber;
                var nr2 = obj2 as SchlipsNumber;

                switch (fn.Type)
                {
                    case SchlipsBuiltin.NumberEqual:
                        result = (nr1 == nr2); 
                        break;
                    case SchlipsBuiltin.GreaterThan:
                        result = (nr1 > nr2); 
                        break;
                    case SchlipsBuiltin.GreaterEqual:
                        result = (nr1 >= nr2); 
                        break;
                    case SchlipsBuiltin.SmallerThan:
                        result = (nr1 < nr2); 
                        break;
                    case SchlipsBuiltin.SmallerEqual:
                        result = (nr1 <= nr2); 
                        break;
                }

                evaluatedObject = (result ? Singletons.True() : Singletons.False());
            }

            return evaluatedObject != null;            
        }


        #endregion



        #region switch environment
        private void PushEnvironment()
        {
            Environment = Environment.SpawnChildEnvironment();
        }

        private void PopEnvironment()
        {
            Environment = Environment.ParentEnvironment;
        }
        #endregion

    }

}
