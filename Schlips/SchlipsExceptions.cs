﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips
{

    public class SchlipsArgumentException : ArgumentException
    {
        public SchlipsArgumentException() : base() { }
        public SchlipsArgumentException(string message) : base(message) { }
        public SchlipsArgumentException(string message, Exception inner) : base(message, inner) { }
    }

    public class SchlipsParserException : ArgumentException
    {
        public SchlipsParserException() : base() { }
        public SchlipsParserException(string message) : base(message) { }
        public SchlipsParserException(string message, Exception inner) : base(message, inner) { }
    }
}
