﻿using Schlips.SchlipsTokenObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schlips
{
    public class SchlipsNode : SchlipsTokenObject, IEnumerable<SchlipsTokenObject>
    {
        string _string;

        public SchlipsTokenObject Head { get; internal set; }
        public SchlipsTokenObject Tail { get; internal set; }

        public int Count
        {
            get
            {
                int result = 0;

                if (!Head.IsNil || !Head.IsAtomic) 
                {
                    result++;

                    if (Tail.IsNode)
                    {
                        result += ((SchlipsNode)Tail).Count;
                    }
                    else if (!Tail.IsNil)
                    {
                        result++;
                    }
                }

                return result;
            }
        }

        
        public SchlipsNode() : this (Singletons.Nil()) {}
        public SchlipsNode(SchlipsTokenObject schlipsObj) : this(schlipsObj, Singletons.Nil()) { IsPair = false; }
        public SchlipsNode(SchlipsTokenObject head, SchlipsTokenObject tail)
        {
            Head = head; Tail = tail;
            IsPair = !Tail.IsNil;
            _string = "";
        }
        

        public SchlipsNode Append(SchlipsTokenObject schlipsObj) 
        {
            if (IsPair)
            {
                throw new SchlipsArgumentException("cannot append to a Pair " + ToString());
            }
            else
            {
                if (this.IsNil)
                {
                    Head = schlipsObj;
                }
                else
                {
                    if (Tail.IsNil && Tail.IsAtomic) //empty SchlipsNode also returns IsNil=true
                    {
                        Tail = new SchlipsNode();
                    }

                    if (!Tail.IsNode) throw new SchlipsArgumentException("cannot append to SchlipsNode "+ ToString());
                    ((SchlipsNode)Tail).Append(schlipsObj);
                }

                //_string += schlipsObj.ToString() + " ";
                _string = "";
            }
            return this;
        }

        
        public IEnumerator<SchlipsTokenObject> GetEnumerator()
        {
            if (this.IsPair)
            {
                yield return Head;
                yield return Tail;
            }
            else if (!this.IsNil)
            {
                yield return this.Head;

                var nextNode = this.Tail as SchlipsNode;

                while (nextNode != null && !nextNode.IsNil)
                {
                    /*if (!nextNode.IsNil)
                    {*/
                        yield return nextNode.Head;
                        /*
                        if (!nextNode.Tail.IsNode)
                        {
                            if (!nextNode.Tail.IsNil)
                            {
                                yield return nextNode.Tail;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {*/
                            nextNode = nextNode.Tail as SchlipsNode;
                        //}
                    /*}
                    else
                    {
                        break;
                    }*/
                }
            }
            
            yield break;
            //yield return Enumerable.Empty<SchlipsTokenObject>();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        public bool ConsistsOf<T>()
        {
            foreach (var node in this)
            {
                if (!(node is T))
                {
                    return false;
                }
            }

            return true;
        }

        public bool ConsistsOf<T>(int expectedCount)
        {
            int count = 0;

            foreach (var node in this)
            {
                if (++count > expectedCount)
                {
                    return false;
                }

                if (!(node is T))
                {
                    return false;
                }
            }

            return count == expectedCount;
        }

        public override string ToString()
        {
            return ToString(false);
        }

        public override string ToString(bool nested)
        {
            _string = "";
            if (IsNil)
            {
                _string = "";
            }
            else if (_string == "")
            {
                if (IsPair)
                {
                    _string = Head.ToString(true) + (Tail.IsNode ? " " : " . ") + Tail.ToString(true);
                }
                else
                {
                    //_string = String.Join(",", this.Select(n => n.ToString()));
                    //*
                    foreach (var element in this)
                    {
                        if (element.IsNode)
                        {
                            _string += ((SchlipsNode)element).ToString(true) + " ";
                        }
                        else
                        {
                            _string += element.ToString() + " ";
                        }
                    }
                    _string = _string.Substring(0, _string.Length - 1);
                    //*/
                }
            }
            return (nested) ? _string : "(" + _string + ")";
        }

	    #region type casting
        public override bool IsNil { get { return Head.IsNil && Head.IsAtomic; } }
        //public override bool IsAtomic { get { return IsNil; } }
        public override bool IsNode { get { return true; } }
        public override bool IsList { get { return Tail.IsNode; } }
        public override bool IsPair { get; protected set; }

        /*
        //it is not allowed to convert from base class
        public static implicit operator SchlipsNode(SchlipsTokenObject obj)
        {
            return (SchlipsNode)obj;
        }//*/
        #endregion
    }
}
