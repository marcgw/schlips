﻿using Schlips.SchlipsTokenObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips
{
    public class SchlipsParser
    {
        public SchlipsEnvironment Environment { get; private set; }

        public SchlipsParser(SchlipsEnvironment environment = null)
        {
            if (environment == null) { environment = SchlipsEnvironmentFactory.RootEnvironment; }
            Environment = environment;
        }

        //TODO: only for compatibility reasons
        public SchlipsTokenObject Parse(SchlipsTokenObject input)
        {
            if (input.IsNil || input.IsAtomic)
            {//SchlipsNil, SchlipsNumber don't need further parsing
                return input;
            }
            else if (input.IsSymbol)
            {//try to evaluate symbol in environment or return symbol                
                return Environment.GetBindingOrSymbol((SchlipsSymbol)input);
            }
            else if (input.IsNode)
            {// might be a Function or just a data node
                return ParseNode((SchlipsNode)input);
            }
            else if (input.IsFunction)
            {//already parsed procedure
                return input;
            }
            else
            {
                throw new SchlipsArgumentException("SchlipsParser cannot handle Parse(" + input.GetType().ToString() + ")");
            }
        }

        private SchlipsTokenObject ParseNode(SchlipsNode node)
        {
            //parse first element -> might be a function
            SchlipsTokenObject firstElement = Parse(node.Head);

            if (firstElement.IsFunction)
            {//if it is a function, parse the tail as function operands
                if (node.Tail.IsNode)
                {
                    return ParseFunction((SchlipsProcedure)firstElement, (SchlipsNode)node.Tail);
                }
                else
                {//or return the function
                    return firstElement;
                }
            }

            else
            {//this is a normal node -> parse all elements in it and return it
                var result = new SchlipsNode();
                result.Append(firstElement);
                SchlipsTokenObject obj;

                if (node.Tail.IsNode)
                {
                    var tail = (SchlipsNode)node.Tail;
                    foreach (var element in tail)
                    {
                        obj = Parse(element);
                        result.Append(obj);
                    }
                }
                return result;
            }

            throw new NotImplementedException("ParseNode("+ node.ToString() +")");
        }

        private SchlipsTokenObject ParseFunction(SchlipsProcedure fn, SchlipsNode operands)
        {
            bool parseFirstOperand = true;

            if (fn.Is(SchlipsBuiltin.Define))
            {
                if (operands.Head.IsNode && !operands.Head.IsNil)
                {//this is the define lambda shorthand
                    return DefineLambdaShorthand(operands);
                }
                //first operand is the identifier, if it is parsed it is not possible to redefine symbols
                parseFirstOperand = false; 
            }
            AppendParametersToFunction((SchlipsProcedure)fn, operands, parseFirstOperand);

            return fn;
        }

        //TODO: remove this function and implement it into the SchlipsLanguage
        private SchlipsTokenObject DefineLambdaShorthand(SchlipsNode operands)
        {
            try
            {
                //TODO rewrite rules to definitions
                var node = operands.Head as SchlipsNode;
                var identifier = (SchlipsSymbol)node.Head;
                var formals = (SchlipsNode)node.Tail;
                var functionBody = Parse(operands.Tail);

                var lambda = (SchlipsUDF)SchlipsBuiltinFactory.Create(SchlipsBuiltin.Lambda);
                lambda.SetExpectedOperandCount(formals.Count);
                lambda.Formals = new List<SchlipsSymbol>(lambda.ExpectedOperands);
                foreach (var formal in formals)
                {
                    lambda.Formals.Add((SchlipsSymbol)formal);
                }
                lambda.Body = functionBody;


                var def = SchlipsBuiltinFactory.Create(SchlipsBuiltin.Define);
                def.Operands.Add(identifier);
                def.Operands.Add(lambda);

                return def;
            }
            catch (Exception)
            {
                throw new SchlipsArgumentException("Define lambda shorthand cannot not be called like that");
            }
        }

        private void AppendParametersToFunction(SchlipsProcedure fn, SchlipsNode operands, bool parseNextOperand = true)
        {
            SchlipsTokenObject parsedParameter = null;

            var isUndefinedLambda = (fn.Is(SchlipsBuiltin.Lambda) && (!((SchlipsUDF)fn).IsDefined));

            foreach (var operand in operands)
            {
                if (!parseNextOperand)
                {
                    parseNextOperand = true;
                    fn.AppendOperand(operand);
                }
                else
                {
                    parsedParameter = Parse(operand);
                    fn.AppendOperand(parsedParameter);
                }
            }

            if (isUndefinedLambda)
            {
                if (((SchlipsUDF)fn).IsDefined)
                {//if this is a lambda and it was not defined before, it should be now
                    return;
                }
                else
                {// otherwise -> not enough arguments passed (if too many arguments are passed an exception is raised before
                    throw new SchlipsArgumentException("Arity mismatch: Lambda expects 2 operands");
                }
            }

            if (!fn.IsExecutable)
            {
                throw new SchlipsArgumentException(String.Format(
                    "Arity mismatch: {0} operands were expected, but {1} were given",
                    fn.ExpectedOperands, operands.Count
                ));
            }
        }
  
    }


   
}
