﻿using Schlips.Helpers;
using Schlips.SchlipsTokenObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips
{
    public class SchlipsReader
    {
        protected const int EOF = -1; //end of line character
        protected const int OpenExpression = (int)'(';
        protected const int CloseExpression = (int)')';

        private int _index = 0;
        private StringBuilder gatheredCharacters;
        private int _peeked = EOF;
        private StringReader inputStream;

        /// <summary>
        /// Contains ' ', '\n', '\r', '\t'
        /// </summary>
        private static List<int> BlankCharacters = new List<int> {(int)' ', (int)'\n', (int)'\r', (int)'\t'};

        //TODO remove peekedChar after debugging
        private char peekedChar = (char)1;

        public SchlipsReader(String input) : this(new StringReader(input)) { }
        
        public SchlipsReader(StringReader input) : this()
        {
            this.inputStream = input;
        }

        public SchlipsReader()
        {
            gatheredCharacters = new StringBuilder();
        }
        
        public SchlipsTokenObject Read(String input)
        {
            return Read(new StringReader(input));
        }

        public SchlipsTokenObject Read(StringReader input)
        {
            this.inputStream = input;
            _index = 0;
            return Read();
        }

        private SchlipsTokenObject Read()
        {
            if (this.inputStream == null) {
                throw new InvalidDataException("inputStream must be set before calling SchlipsReader.Read()");
            }
            
            var result = ReadToken();

            if (_peeked != EOF)
            {
                throw new SchlipsReaderException("unexpected character '" + (char)_peeked + "' after '" + result.ToString() + "' in input.");
            }

            return result;
        }

        private void SkipBlanks()
        {
            PeekCharacter();
            while (BlankCharacters.Contains(_peeked))
            //while (_peeked == (int)' ' || _peeked == (int)'\n' || _peeked == (int)'\r')
            {
                ReadAndPeekCharacter();
            }
        }

        private int ReadAndPeekCharacter()
        {
            ReadCharacter();
            return PeekCharacter();   
        }

        private int ReadCharacter()
        {
            var result = inputStream.Read();
            _index++;
            PeekCharacter();
            return result;
        }

        private int PeekCharacter()
        {
            _peeked = inputStream.Peek();
            //TODO remove after debugging
            peekedChar = (char)_peeked;
            return _peeked;
        }

        private SchlipsTokenObject ReadToken()
        {
            SchlipsTokenObject result = null;
            SkipBlanks();

            SchlipsTokenObject makro = ExecuteReaderMakros();
            if (makro != null) return makro;

            if (_peeked == OpenExpression)
            {
                result = ReadNode();
            }
            else if (CharacterIsDigit(_peeked))
            {
                result = ReadNumber();
            }
            else if (_peeked == (int)'-')
            {
                //might be a integerPart or a symbol (operand.g. builtin-minus)
                //-> command next character
                gatheredCharacters.Append((char)inputStream.Read());

                _peeked = inputStream.Peek();
                if (CharacterIsDigit(_peeked))
                {
                    result = ReadNumber(negative: true);
                }
                else
                {
                    result = ReadSymbol();
                }
            }
            else if (_peeked == (int)'.')
            {
                throw new SchlipsReaderException("Cannot create dotted lists yet");
            }
            else
            {
                result = ReadSymbol();
            }


            if (result == null)
            {
                throw new SchlipsReaderException(gatheredCharacters);
            }

            gatheredCharacters.Clear();
            SkipBlanks();
            return result;   
        }

        //TODO: use environment to define reader makros
        private SchlipsTokenObject ExecuteReaderMakros()
        {
            if (_peeked == (int)'\'')//int=39
            {
                var quote = new SchlipsNode(new SchlipsSymbol("quote"));
                ReadAndPeekCharacter();
                quote.Append(ReadToken());

                return quote;
            }
            else
            {
                var peekedChar = (char)_peeked;
            }

            return null;
        }

        private bool CharacterIsDigit(int c)
        {
            //Char.IsDigit((char)peeked)
            return (c >= (int)'0' && c <= (int)'9');
        }

        private SchlipsNumber ReadNumber(bool negative=false)
        {
            bool isFloat = false;
            System.Numerics.BigInteger number = 0;
            int floatingPart = 0;
            byte digit = 0; byte floatingDigits = 0;
            //Type numberClass = typeof(Int32);
            
            while (!CharacterIsSeparator(_peeked))
            {
                gatheredCharacters.Append((char)_peeked);

                if (CharacterIsDigit(_peeked))
                {
                    digit = (byte)(_peeked - (int)'0');
                    /*
                    integerPart = GenericMath.Multiply(integerPart, 10);
                    integerPart = GenericMath.Add(integerPart, (peeked - (int)'0'));
                    */
                    //INFO: checked throws System.OverflowException
                    if (!isFloat)
                    {
                        number *= 10;
                        number += digit;
                    }
                    else
                    {
                        floatingPart *= 10;
                        floatingPart += digit;
                        floatingDigits++;
                    }
                }
                else if (_peeked == (int)'.' && !isFloat)
                {
                    isFloat = true;
                }
                else
                {
                    throw new SchlipsReaderException();
                }

                ReadAndPeekCharacter();
            }

            if (!isFloat)
            {
                if (negative) number *= -1;
                return CreateIntegerSchlipsNumber(number);
            }
            else
            {//convert floating part at the end to minimize delta problems with floating numbers
                var exp = Math.Pow(10, floatingDigits);
                double dbl = (double)number + (floatingPart/exp);
                if (negative) dbl *= -1;
                return new SchlipsNumber(dbl);
            }
        }

        private SchlipsNumber CreateIntegerSchlipsNumber(System.Numerics.BigInteger integerPart)
        {
            if (integerPart > Int16.MinValue && integerPart < Int16.MaxValue)
            {
                return new SchlipsNumber((Int16)integerPart);
            }
            if (integerPart > Int32.MinValue && integerPart < Int32.MaxValue)
            {
                return new SchlipsNumber((Int32)integerPart);
            }
            else
            {
                return new SchlipsNumber((Int64)integerPart);
            }
        }

        private SchlipsSymbol ReadSymbol()
        {
            while (!CharacterIsSeparator(_peeked))
            {
                gatheredCharacters.Append((char)_peeked);
                ReadAndPeekCharacter();
            }

            if (gatheredCharacters.Length > 0)
            {
                return new SchlipsSymbol(gatheredCharacters.ToString());
            }

            throw new SchlipsReaderException(gatheredCharacters);
        }

        private bool CharacterIsSeparator(int peeked)
        {
            return 
                peeked == EOF || BlankCharacters.Contains(peeked) ||
                peeked == OpenExpression || peeked == CloseExpression;
        }


        private SchlipsNode ReadNode() 
        {
            if (_peeked != OpenExpression)
            {
                throw new SchlipsReaderException("A SchlipsNode must Start with '(', but the first Character was '"+(char)_peeked+"'");
            }

            ReadAndPeekCharacter();
            SkipBlanks();

            SchlipsNode result = new SchlipsNode();

            while (_peeked != CloseExpression)
            {
                result.Append(ReadToken());
             
                if (_peeked == EOF)
                {
                    throw new SchlipsReaderException("The closing ')' is missing.");
                }
            }

            if (_peeked != CloseExpression)
            {
                throw new SchlipsReaderException("A SchlipsNode must end with ')', but the last Character was '" + (char)_peeked + "'");
            }

            ReadAndPeekCharacter();

            return result;
        }

        
    }

    public class SchlipsReaderException : ArgumentException
    {
        public SchlipsReaderException() { }
        public SchlipsReaderException(string message) : base(message) { }
        public SchlipsReaderException(string message, Exception inner) : base(message, inner) { }
        public SchlipsReaderException(StringBuilder gatheredCharacters)
            : base(String.Format("SchlipsReader failed to command Token '{0}'", gatheredCharacters.ToString())) { }
    }
}
