﻿using Schlips.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Schlips.SchlipsTokenObjects
{
	public enum NumberTypes {
		Byte, Sbyte,
		Int16, Int32, Int64, BigInt,
		Float, Double
	}

    public class SchlipsTokenObject
    {
        public Object Value { get; set; }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public virtual string ToString(bool nested) { return ToString(); }
        public override string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object obj)
        {
            var schlipsTokenObj = obj as SchlipsTokenObject;
            if (schlipsTokenObj != null)
            {
                return Value.Equals(schlipsTokenObj.Value);
            }

            return base.Equals(obj);
        }

        #region type casting
        public virtual bool IsAtomic { get { return false; } }
        public virtual bool IsTrue { get { return false; } }
        public virtual bool IsFalse { get { return true; } }

        public virtual bool IsSymbol { get { return false; } }
        public virtual bool IsNumber { get { return false; } }
        
        public virtual bool IsNode { get { return false; } }
        public virtual bool IsNil { get { return false; } }
        public virtual bool IsList { get { return false; } }
        public virtual bool IsPair { get { return false; } protected set {} }

        public virtual bool IsFunction { get { return false; } }
        public virtual bool IsExecutable { get { return false; } }
        #endregion

        public virtual SchlipsTokenObject Retrieve()
        {
            return this;
        }        
    }

}
