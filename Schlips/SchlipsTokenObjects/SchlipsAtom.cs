﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips.SchlipsTokenObjects
{
    public class SchlipsAtom : SchlipsTokenObject
    {
        public SchlipsAtom(SchlipsBuiltin type)
        {
            Value = type;
        }

        #region type casting
        public override bool IsAtomic { get { return true; } }
        public override bool IsTrue { get { return ((int)Value == (int)SchlipsBuiltin.True); } }
        public override bool IsFalse { get { return !IsTrue; } }
        public override bool IsNil { get { return ((int)Value == (int)SchlipsBuiltin.Nil); } }
        #endregion
    }

}
