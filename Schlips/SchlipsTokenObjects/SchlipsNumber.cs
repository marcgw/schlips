﻿using Schlips.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips.SchlipsTokenObjects
{
    public class SchlipsNumber : SchlipsTokenObject
    {
        public NumberTypes NumberType { get; protected set; }

        //private System.Numerics.BigInteger result;
        //private object result1;

        public SchlipsNumber(Int32 number)
        {
            NumberType = NumberTypes.Int32;
            this.Value = number;
        }

        public SchlipsNumber(Int64 number)
        {
            NumberType = NumberTypes.Int64;
            this.Value = number;
        }

        public SchlipsNumber(float number)
        {
            NumberType = NumberTypes.Float;
            this.Value = number;
        }
        public SchlipsNumber(double number)
        {
            NumberType = NumberTypes.Double;
            this.Value = number;
        }

        public SchlipsNumber(System.Numerics.BigInteger number)
        {
            NumberType = NumberTypes.BigInt;
            this.Value = number;
        }

        public SchlipsNumber(object number)
        {
            if (GenericMath.IsNumeric(number))
            {
                if (!GenericMath.IsInteger(number))
                {
                    this.Value = Convert.ToDouble(number);
                    this.NumberType = NumberTypes.Double;
                }
                else
                {
                    InitializeWithIntegerNumber(number);
                }
            }
            else
            {
                throw new NotImplementedException("Cannot create a SchlipsNumber with '" + number + "' of type '" + number.GetType().ToString() + "'.");
            }
        }

        private void InitializeWithIntegerNumber(object number)
        {
            try
            {
                checked
                {
                    this.Value = Convert.ToInt64(number);
                    this.NumberType = NumberTypes.Int64;
                }
            }
            catch (System.OverflowException)
            {
                checked
                {
                    this.Value = Convert.ToDouble(number);
                    this.NumberType = NumberTypes.Double;
                }
            }
        }


        public static SchlipsNumber operatorPlus1(SchlipsNumber n1, SchlipsNumber n2)
        {
            double result = Convert.ToDouble(n1.Value) + Convert.ToDouble(n2.Value);

            Int64 resultInt = (Int64)result;
            if (Math.Abs(result - resultInt) <= double.Epsilon)
            {
                return new SchlipsNumber(resultInt);
            }

            return new SchlipsNumber(result);
        }

        public static SchlipsNumber operator +(SchlipsNumber n1, SchlipsNumber n2)
        {
            return GenericMath.Add(n1, n2);
            //return operatorPlus1(n1, n2);
        }

        public static SchlipsNumber operator -(SchlipsNumber n1, SchlipsNumber n2)
        {
            // TODO Add Generic operator
            //return GenericMath.Subtract(n1, n2);
            checked
            {
                double result = Convert.ToDouble(n1.Value) - Convert.ToDouble(n2.Value);

                Int64 resultInt = (Int64)result;
                if (Math.Abs(result - resultInt) <= double.Epsilon)
                {
                    return new SchlipsNumber(resultInt);
                }

                return new SchlipsNumber(result);
            }
        }

        public static SchlipsNumber operator *(SchlipsNumber n1, SchlipsNumber n2)
        {
            // TODO Add Generic operator
            //return GenericMath.Multiply(n1, n2);
            checked
            {
                double result = Convert.ToDouble(n1.Value) * Convert.ToDouble(n2.Value);

                Int64 resultInt = (Int64)result;
                if (Math.Abs(result - resultInt) <= double.Epsilon)
                {
                    return new SchlipsNumber(resultInt);
                }

                return new SchlipsNumber(result);
            }
        }

        public static SchlipsNumber operator /(SchlipsNumber n1, SchlipsNumber n2)
        {
            // TODO Add Generic operator
            //return GenericMath.Multiply(n1, n2);
            checked
            {
                double result = Convert.ToDouble(n1.Value) / Convert.ToDouble(n2.Value);

                Int64 resultInt = (Int64)result;
                if (Math.Abs(result - resultInt) <= double.Epsilon)
                {
                    return new SchlipsNumber(resultInt);
                }

                return new SchlipsNumber(result);
            }
        }



        #region compare operators
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public static bool operator ==(SchlipsNumber n1, SchlipsNumber n2)
        {
            var isNull = false;
            var result = false;
            double v1 = 0, v2 = 0;

            try
            {
                v1 = Convert.ToDouble(n1.Value);
            }
            catch (NullReferenceException) { isNull = true; }

            try
            {
                v2 = Convert.ToDouble(n2.Value);
                result = isNull ? false : (v1 == v2);
            }
            catch (NullReferenceException) { result = isNull ? true : false; }

            /*
            if (Object.ReferenceEquals(n1, n2)) { return true; }
            else if (Object.ReferenceEquals(n1, null) && Object.ReferenceEquals(n2, null)) { return true; }
            else {
                return (Convert.ToDouble(n1.Value) == Convert.ToDouble(n2.Value));
            }*/

            return result;
        }

        public static bool operator !=(SchlipsNumber n1, SchlipsNumber n2)
        {
            return !(n1 == n2);
        }

        public static bool operator <(SchlipsNumber n1, SchlipsNumber n2)
        {
            return Convert.ToDouble(n1.Value) < Convert.ToDouble(n2.Value);
        }

        public static bool operator >(SchlipsNumber n1, SchlipsNumber n2)
        {
            return n2 < n1;
        }

        public static bool operator <=(SchlipsNumber n1, SchlipsNumber n2)
        {
            return !(n1 > n2);
        }

        public static bool operator >=(SchlipsNumber n1, SchlipsNumber n2)
        {
            return !(n1 < n2);
        }
        #endregion



        #region type casting
        public override bool IsAtomic { get { return true; } }
        public override bool IsNumber { get { return true; } }
        #endregion
    }
}
