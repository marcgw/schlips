﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schlips.SchlipsTokenObjects
{
    public class SchlipsSymbol : SchlipsTokenObject
    {
        public SchlipsSymbol(string symbolname)
        {
            this.Value = symbolname;
        }

        #region type casting
        public override bool IsSymbol { get { return true; } }
        #endregion
    }
}
