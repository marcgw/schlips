using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Schlips;
using Schlips.SchlipsTokenObjects;

namespace SchlipsCLI
{
    public class RPEL
    {
        private SchlipsReader r;
        private SchlipsParser p;
        private SchlipsEvaluator e;

        public bool Debug { get; private set; }
        

        public RPEL()
        {
            InitializeREPL();
        }

        private void InitializeREPL()
        {
            SchlipsEnvironmentFactory.GenerateRootEnvironment();
            r = new SchlipsReader();
            p = new SchlipsParser();
            e = new SchlipsEvaluator(p);
        }

        
        private string divider = new String('-', 78);

        public void Start(bool debug=false)
        {
            Debug = debug;
            Console.WriteLine("starting schlips-cli");
            LoadLanguageFile(@"lisp\lang-scheme.lisp");

            PrintGreeting();

            ReadLoop();
        }



        private void LoadLanguageFile(string filename)
        {
            var needToggle = !Debug;
            Console.WriteLine();
            if (needToggle) ToggleDebug();
            InitializeREPL();
            EvaluateFile(filename);
            if (needToggle) ToggleDebug();
            Console.WriteLine();
        }


        private void EvaluateFile(string filename)
        {
            try
            {
                Console.WriteLine("Evaluating file '" + filename + "':");
                foreach (var cmd in ReadCommandsFromFile(filename))
                {
                    Evaluate(cmd);
                }
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException || ex is IOException)
                {//file reading errors
                    Console.WriteLine(ex.Message);
                }
                else throw;
            }
        }

        //Hint: this does not allow comments
        private IEnumerable<string> ReadCommandsFromFile(string filename)
        {
            string line;
            using (var reader = File.OpenText(filename))
            {
                var cmd = "";
                while ((line = reader.ReadLine()) != null)
                {
                    {
                        if (cmd.Length == 0)
                        {
                            cmd = line.Trim();
                        }
                        else
                        {
                            cmd += " " + line.Trim();
                        }

                        if (!CommandNotFinished(cmd))
                        {
                            yield return cmd;
                            cmd = "";
                        }
                    }
                }
            }
        }


        public void PrintGreeting()
        {
            var greeting = "Welcome to Schlips Command Line Interpreter (schlips-cli)";

            Console.WriteLine("\n"+divider+"\n"+divider);
            Console.WriteLine(greeting);
            Console.WriteLine("\nInformation about usage:\n  Enter ':q' to quit or\n  ':h' to show all available commands for the command line interface\n");
            Console.WriteLine("Current Directory is: "+ Directory.GetCurrentDirectory());
            Console.WriteLine(divider+"\n");
        }


        public void ReadLoop()
        {
            string command = "";
            string output = "";

            while (true)
            {
                //line multiline
                command = ReadCommandFromConsole();
                if (IsExitCommand(command)) { break; }

                if (!IsReplSpecialCommand(command))
                {
                    output = Evaluate(command);
                    Console.WriteLine(output);
                }
            }
        }

        #region line command
        private string ReadCommandFromConsole()
        {
            string read = "", line = "";
            do
            {
                Console.Write((line == "" ? "> " : "  ")); // write caret or indentation

                line = Console.ReadLine();
                if (line.Length > 0)
                {
                    read += line + " ";
                }
            } while (read.Length==0 || CommandNotFinished(read));

            return read.Substring(0, read.Length - 1); //trim trailing space
        }

        private bool CommandNotFinished(string read)
        {
            var openingBrackets = CountOccurrences(read, (int)'(');
            var closingBrackets = CountOccurrences(read, (int)')');

            return openingBrackets != closingBrackets;
        }

        private int CountOccurrences(string read, int needle)
        {
            int counter = 0;
            foreach (char c in read)
            {
                if (c == needle) { counter++; }
            }
            return counter;
        }
        #endregion

        private bool IsExitCommand(string cmd)
        {
            return (cmd == "exit" || cmd == "quit" || cmd == "logout" || cmd == ":q");
        }

        private bool IsReplSpecialCommand(string cmd)
        {
            if (cmd.StartsWith(":c")) //:commands || :c
            {
                PrintAllCommands();
            }
            else if (cmd.StartsWith(":d")) //:debug || :d
            {
                ToggleDebug();
            }
            else if (cmd.StartsWith(":e")) //:evaluate || :e
            {
                try
                {
                    EvaluateFile(cmd.Split(' ')[1]);
                }
                catch (Exception ex)
                {
                    if (ex is IndexOutOfRangeException || ex is ArgumentException)
                    {//no filename was passed (or it is not readable)
                        Console.WriteLine("Error: Could not load file ''. Please specify a relative or absolute file");
                    }
                    else throw;
                }
            }
            else if (cmd.StartsWith(":h")) //:help || :h
            {
                PrintHelp();
            }
            else if (cmd.StartsWith(":l")) //:load || :l
            {
                LoadLanguageSelector();
            }
            else if (cmd.StartsWith(":r")) //:reset || :r
            {
                Console.WriteLine("\nReset interpreter");
                InitializeREPL();
                Console.WriteLine(divider);
            }
            else
            {
                return false;
            }

            return true;
        }

        private void PrintAllCommands()
        {
            Console.WriteLine(divider + "\nAvailable commands in root environment:");
            foreach (KeyValuePair<SchlipsSymbol, SchlipsTokenObject> entry in e.Environment.bindings)
            {
                Console.WriteLine(" - {0} -> {1}", entry.Key, entry.Value);
            }
            Console.WriteLine(divider + "\n");
        }


        private void ToggleDebug()
        {
            Debug = !Debug;
            const string offState = "off. Commands will be evaluated without printing.";
            const string onState = "on. All commands will be printed before evaluating them.";
            Console.WriteLine("INFO: Debug Mode was turned " + (Debug ? onState : offState));
        }


        private void PrintHelp()
        {
            string[] commands = {
                "':c'/':commands' -> print all commands available in the environment",
                "':d'/':debug' -> toggle debug mode",
                "':e'/':evaluate' <Filename> -> evaluate file",
                "':h'/':help' -> print this information",
                "':l'/':load' -> load another language",                    
                "':r'/':reset' -> delete environment and reload scheme language",
                "':q'/':quit' -> quit this interpreter"
            };

            Console.WriteLine("Available special commands:");
            foreach (var command in commands)
            {
                Console.WriteLine("  - " + command);
            }

            Console.WriteLine(divider + "\n");
        }

        //load and evaluate a language definition file
        // (just a bunch of defines)
        private void LoadLanguageSelector()
        {
            const string languagePattern = "lang-*.lisp";
            const string languagePatternRegex = @"lang-(\w+)\.lisp";

            string[] paths = Directory.GetFiles("lisp", languagePattern);
            Regex re = new Regex(languagePatternRegex, RegexOptions.IgnoreCase);
            Match m;

            if (paths.Length == 0)
            {// abort if no file found
                Console.WriteLine("Error: Could not find any languages in subfolder 'lisp' (files must match pattern '" + languagePattern + "')");
                return;
            }

            var result = "";

            do
            {
                Console.WriteLine("Available languages: ");
                for (int i=0; i<paths.Length; i++)
                {
                    //Console.WriteLine(path);
                    m = re.Match(paths[i]);
                    //Console.WriteLine("  " + m.Groups[1]);
                    Console.WriteLine("  {0}: {1}", i+1, m.Groups[1]);
                }

                Console.Write("Select a language (Between 1 and {0})): ", paths.Length);
                var selection = Console.ReadLine();
                int sel;

                if (int.TryParse(selection, out sel))
                {
                    if (sel > 0 && sel <= paths.Length)
                    {
                        result = paths[sel - 1];
                    }
                }
                else
                {
                    if (IsExitCommand(selection)) return;
                }
            } while (result == "");

           LoadLanguageFile(result);
        }


        private string Evaluate(string input)
        {
            SchlipsTokenObject rObj, pObj, eObj;
            string result = "";

            if (Debug) { Console.WriteLine("Evaluating `" + input + "`"); }

            try
            {
                rObj = r.Read(input);
                pObj = p.Parse(rObj);
                eObj = e.Evaluate(pObj);

                result = eObj.ToString();
            }
            catch (SchlipsArgumentException ex)
            {
                result = ex.Message;
            }
            catch (Exception ex)
            {
                result = ex.Message;
                //throw;
            }

            return result;
        }


        public static void Main(string[] args)
        {
            var repl = new RPEL();
            repl.Start(debug:false);
            
            // Keep the console window open
            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }

    }
}

