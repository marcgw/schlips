﻿
using NUnit.Framework;
using System;
using Schlips;
using Schlips.SchlipsTokenObjects;

namespace SchlipsTest
{
    public class SchlipsTester
    {
        protected SchlipsReader reader;
        protected SchlipsParser parser;
        protected SchlipsEvaluator eval;

        protected virtual void Init()
        {
            reader = new SchlipsReader();
            //reset bindings in root environment that is used both by SchlipsParser and SchlipsEvaluator
            SchlipsEnvironmentFactory.GenerateRootEnvironment();
            parser = new SchlipsParser();
            eval = new SchlipsEvaluator(parser);
        }


        protected void AssertHelper<T>(SchlipsTokenObject actual, object expected)
        {
            Assert.IsInstanceOf<T>(actual);
            Assert.AreEqual(Convert.ToString(expected), Convert.ToString(actual.Value));
        }

        protected void AssertException<T>(string input)
        {
            Assert.Throws<SchlipsArgumentException>(() => Evaluate(input), "Evaluate('" + input + "') should throw an exception");
        }

        protected SchlipsTokenObject Evaluate(string input)
        {
            var read = reader.Read(input);
            var parsed = parser.Parse(read);
            var evaluated = eval.Evaluate(parsed);

            return evaluated;
        }

        protected void AssertNode(string input, int count)
        {
            var node = Evaluate(input);
            Assert.IsInstanceOf<SchlipsNode>(node);
            Assert.AreEqual(count, ((SchlipsNode)node).Count);
        }

        protected void AssertNode(SchlipsTokenObject node, int count)
        {
            Assert.IsInstanceOf<SchlipsNode>(node);
            Assert.AreEqual(count, ((SchlipsNode)node).Count);
        }

        protected void CheckStringAssertions(string[] assertions)
        {
            string input, actual, expected;
            SchlipsTokenObject obj;

            for (int i=0; i<assertions.Length; i+=2)
            {
                input = assertions[i]; expected = assertions[i+1];

                obj = Evaluate(input);
                actual = obj.ToString();

                Assert.AreEqual(expected, actual);
            }
        }

    }


}
