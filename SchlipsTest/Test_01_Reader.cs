﻿
using NUnit.Framework;
using System;
using Schlips;
using Schlips.SchlipsTokenObjects;

namespace SchlipsTest
{
    [TestFixture]
    class Test_01_Reader
    {
        SchlipsReader reader;
        SchlipsNode node;
        SchlipsNumber number;
        SchlipsSymbol symbol;
        SchlipsTokenObject obj;

        [SetUp]
        public void init()
        {
            reader = new SchlipsReader();
            node = null;
            number = new SchlipsNumber(-1);
            symbol = new SchlipsSymbol("");
            obj = null;
        }

        private SchlipsNode ReadNode(string input)
        {
            var possibleNode = reader.Read(input);
            Assert.IsInstanceOf<SchlipsNode>(possibleNode);

            return (SchlipsNode)possibleNode;
        }

        private void AssertHelper<T>(object passed)
        {
            var input = Convert.ToString(passed);
            var output = reader.Read(input);
            Assert.IsInstanceOf<T>(output);
            Assert.AreEqual(input, Convert.ToString(output.Value));
        }

        [Test]
        public void Read_01_ReadANumber() 
        {
            obj = reader.Read("12");
            Assert.IsInstanceOf<SchlipsNumber>(obj);
            var twelve = (SchlipsNumber)obj;

            Assert.AreEqual(12, twelve.Value);
        }

        [Test]
        public void Read_01b_ReadAFloatNumber()
        {
            obj = reader.Read("12.81");
            Assert.IsInstanceOf<SchlipsNumber>(obj);
            var twelve = (SchlipsNumber)obj;

            Assert.AreEqual(12.81, twelve.Value);
        }

        [Test]
        public void Read_02_ReadNumbers()
        {
            object[] numbers = {
                33, -22, 46, 193842823, -92234428, 
                Int32.MaxValue, Int32.MinValue,
                Int64.MaxValue, Int64.MinValue,
                "-17"
            };

            foreach (var n in numbers)
            {
                AssertHelper<SchlipsNumber>(n);
            }
        }

        [Test]
        public void Read_02b_ReadFloatNumbers()
        {
            string[] numbers = {
                "-193.38289", "0.3719", "718188.2", "93839.302"
            };
            SchlipsTokenObject output;

            foreach (string n in numbers)
            {
                output = reader.Read(n);
                Assert.IsInstanceOf<SchlipsNumber>(output);
                Assert.AreEqual(n, Convert.ToString(output.Value).Replace(',','.'));
            }
        }

        [Test]
        public void Read_03_ReadASymbol()
        {
            var input = "horst";
            obj = reader.Read(input);
            Assert.IsInstanceOf<SchlipsSymbol>(obj);
            Assert.AreEqual(input, obj.Value);
        }

        [Test]
        public void Read_04_ReadSymbols()
        {
            string[] symbols = {
                "+", "-", "-a1", "as-77", "b008fQ", "define",
                "lambda", "q_17", "thisIsQuiteALongSymbolToken", 
                "Akdobioelad#-sdoaepü+qajlcyu193ad.y123050ky.jjazzzdiaoAAKL"
            };

            foreach (var s in symbols)
            {
                AssertHelper<SchlipsSymbol>(s);
            }
        }

        [Test]
        public void Read_05_ReaderExceptions()
        {
            string message;

            string[] illegalInput = {
                "a(b", "(.)", "(ab d98 99)4",
                "( () ()))", "( () (   ) )   )",
                "( abc ()", "(a()"
            };

            string[] legalInput = {
               "()", "( abc () )", "(   a () )", "17", "lambda"
            };

            
            foreach (var input in illegalInput) 
            {
                message = "input '" + input + "' should raise a SchlipsReaderException";
                Assert.Throws<SchlipsReaderException>(() => reader.Read(input), message);
            }

            foreach (var input in legalInput)
            {
                message = "input '" + input + "' should not raise a SchlipsReaderException";
                Assert.DoesNotThrow(() => reader.Read(input), message);
            }
        }

        [Test]
        public void Read_06_ReadANode()
        {
            ReadNode("(+ 1 2)");
        }

        [Test]
        public void Read_07_ReadNodes()
        {
            ReadNode("()");
            ReadNode("( () () (2 38))");
            ReadNode("(lambda (n) (+ n 1))");
        }

        [Test]
        public void Read_08_ReadNodeWithSameElements()
        {
            node = ReadNode("( abc   hello h171 h-2)");
            Assert.IsTrue(node.ConsistsOf<SchlipsSymbol>(4), "node should contain 4 SchlipsSymbols");

            node = ReadNode("(17 22 -39941002)");
            Assert.IsTrue(node.ConsistsOf<SchlipsNumber>(3), "node should contain 3 SchlipsNumbers");

            node = ReadNode("( (  ) () (aber))");
            Assert.IsTrue(node.ConsistsOf<SchlipsNode>(3), "node should contain 3 SchlipsNodes");

            node = ReadNode("( () (lambda   )   (ab er -23  ) (+ 33 12))");
            Assert.IsTrue(node.ConsistsOf<SchlipsNode>(4), "node should contain 4 SchlipsNodes");
        }

        [Test]
        public void Read_10_ReaderMakroQuote_Number()
        {
            obj = reader.Read("'10");
            Assert.IsInstanceOf<SchlipsNode>(obj);
        }

        [Test]
        public void Read_10b_ReaderMakroQuote_Cons()
        {
            obj = reader.Read("'(10)");
            Assert.IsInstanceOf<SchlipsNode>(obj);
        }

        [Test]
        public void Read_10c_ReaderMakroQuote_Lambda()
        {
            obj = reader.Read("'(lambda () (+ 3 4))");
            Assert.IsInstanceOf<SchlipsNode>(obj);
        }
       
    }
}
