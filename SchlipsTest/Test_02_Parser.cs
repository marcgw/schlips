﻿
using NUnit.Framework;
using System;
using Schlips;
using Schlips.SchlipsTokenObjects;

namespace SchlipsTest
{
    [TestFixture]
    class Test_02_Parser
    {
        SchlipsReader reader;
        SchlipsParser parser;

        SchlipsNode node;
        SchlipsTokenObject obj;
        SchlipsNumber number;
        SchlipsSymbol symbol;

        [SetUp]
        public void init()
        {
            reader = new SchlipsReader();
            parser = new SchlipsParser();
            
            node = null;
            number = new SchlipsNumber(-1);
            symbol = new SchlipsSymbol("");
        }


        [Test]
        public void Parse_01_Numbers()
        {
            obj = reader.Read("12");
            var result = parser.Parse(obj);

            Assert.IsInstanceOf<SchlipsNumber>(result);
            Assert.AreEqual(12, result.Value);
        }

        [Test]
        public void Parse_02_NumberInCons()
        {
            obj = reader.Read("(12)");
            var result = parser.Parse(obj);

            Assert.IsInstanceOf<SchlipsNode>(result);
            node = (SchlipsNode)result;
            Assert.IsInstanceOf<SchlipsNumber>(node.Head);

            Assert.AreEqual(12, node.Head.Value);
        }

        /**
         * further tests of the Parser Object are done in Test_04_Basic and Test_05_Extended
         */
    }
}
