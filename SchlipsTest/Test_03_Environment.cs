using NUnit.Framework;
using System;
using Schlips;
using Schlips.SchlipsTokenObjects;

namespace SchlipsTest
{
    [TestFixture]
    public class Test_03_Environment
    {
        private SchlipsEnvironment environment;

        [SetUp]
        public void Init()
        {
            environment = new SchlipsEnvironment();
        }

        [Test]
        public void Environment_01_GetMissingBinding()
        {
            var a = new SchlipsSymbol("a");
            var result = environment.GetBindingOrNull(a);

            Assert.AreEqual(null, result);
        }

        [Test]
        public void Environment_02_SetAndGetBinding()
        {
            var a = new SchlipsSymbol("a");
            var ten = new SchlipsNumber(10);
            environment.SetBinding(a, ten);

            var result = environment.GetBindingOrNull(a);
            Assert.IsInstanceOf<SchlipsNumber>(result);

            var expected = "10";
            var actual = result.ToString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Environment_03_SetAndGetBinding()
        {
            var a = new SchlipsSymbol("a");
            var ten = new SchlipsNumber(10);
            environment.SetBinding(a, ten);

            var a2 = new SchlipsSymbol("a");

            var result = environment.GetBindingOrNull(a2);
            Assert.IsInstanceOf<SchlipsNumber>(result);

            var expected = "10";
            var actual = result.ToString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Environment_04_OverwriteBinding()
        {
            var a = new SchlipsSymbol("a");
            var ten = new SchlipsNumber(10);
            environment.SetBinding(a, ten);
            Assert.AreEqual("10", environment.GetBindingOrNull(a).ToString());

            var a2 = new SchlipsSymbol("a");
            var four = new SchlipsNumber(4);
            environment.SetBinding(a2, four);

            Assert.AreEqual("4", environment.GetBindingOrNull(a).ToString());
        }
    }
}

