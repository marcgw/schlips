﻿
using NUnit.Framework;
using System;
using Schlips;
using Schlips.SchlipsTokenObjects;

namespace SchlipsTest
{
    [TestFixture]
    class Test_04_Basic : SchlipsTester
    {
        SchlipsNumber number;
        SchlipsSymbol symbol;
        
        [SetUp]
        protected override void Init()
        {
            base.Init();
            number = new SchlipsNumber(-1);
            symbol = new SchlipsSymbol("");
        }

        
        #region helper functions
        
        private void CheckAssertions<T>(string[] assertions)
        {
            string input, expected, actual;
            SchlipsTokenObject evaluatedObject;

            for (int i = 0; i < assertions.Length; i += 2)
            {
                input = assertions[i];
                expected = assertions[i + 1];

                evaluatedObject = Evaluate(input);
                Assert.IsInstanceOf<T>(evaluatedObject);

                actual = evaluatedObject.ToString();

                Assert.AreEqual(expected, actual, String.Format(
                    "{0} should evaluate to {1}, but {2} was returned",
                    input, expected, actual)
                );
            }
        }
        #endregion


        #region math functions
        [Test]
        public void Eval_01_SimpleAdd()
        {
            var input = "(+ 1 2)";
            int expected = 3;

            SchlipsTokenObject obj = Evaluate(input);
            AssertHelper<SchlipsNumber>(obj, expected);
        }

        [Test]
        public void Eval_01b_Add_SimpleNested()
        {
            var input ="(+ 2 (+ 1 1))";
            int expected = 4;

            SchlipsTokenObject obj = Evaluate(input);
            AssertHelper<SchlipsNumber>(obj, expected);
        }

        [Test]
        public void Eval_01c_Add_Extended()
        {
            string[] calculations =
			{
				"(+ -1 2)", "1",
				"(+ 3 (+ 1 -2))", "2",
				"(+ 1 -17 1)", "-15",
				"(+ (+ 2 -7) (+ (+ -2 3) (+ 2 3)) -1)", "0"
			};

            CheckAssertions<SchlipsNumber>(calculations);
        }
        [Test]
        public void Eval_02_Subtract_Simple()
        {
            var input = "(- 3 2)";
            int expected = 1;

            SchlipsTokenObject obj = Evaluate(input);
            AssertHelper<SchlipsNumber>(obj, expected);
        }

        [Test]
        public void Eval_03_AddSubtract()
        {
            string[] calculations = {
				"(+ 4 2)", "6",
				"(+ 1 2 3 4)", "10",
				"(+ -3 7)", "4",
				"(+ 4 -12)", "-8",
				"(+ 1 (+    2 3)   -1)", "5",
				"(+ -1 (+ 5    2))", "6",
				"(+ 77 1000)", "1077",
				"(- 3 1)", "2",
				"(+ (+ 3 4) (- 7 2))", "12",
				"(- (+ 3 4) (- 7 2))", "2",
				"(- (- 1 2) (- 2 3))", "0",
				"(- (- -1 2) (- 2 (+ 7 -3)))", "-1"
			};

            CheckAssertions<SchlipsNumber>(calculations);
        }

        [Test]
        public void Eval_04_SimpleMultiply()
        {
            var input = "(* 3 2)";
            int expected = 6;

            SchlipsTokenObject obj = Evaluate(input);
            AssertHelper<SchlipsNumber>(obj, expected);
        }

        /*
         * TODO wieder einfügen
         * /
        [Test]
        public void Eval_06_SimpleDivide()
        {
            Assert.Fail();
        }*/

        [Test]
        public void Eval_07_MathWithVariables()
        {
            Evaluate("(define ten 10)");
            Evaluate("(define mTwelve -12)");

            Evaluate("(define four 4)");
            Evaluate("(define fourteen (+ four ten))");


            string[] assertions = {
				"(- ten 3)", "7",
				"(+ 4 (- mTwelve      ten))", "-18",
				"(+ ten mTwelve)", "-2",
				"(- (- ten 2) (- 2 (+ 7 -3   )) )", "10",
				"(+ fourteen ten)", "24",
                "(+ 10000 (- 2 mTwelve) 991102)", "1001116"
			};

            CheckAssertions<SchlipsNumber>(assertions);
        }

        [Test]
        public void Eval_08_MathWithFloatNumbers()
        {
            string[] assertions = {
				/*"(- 3.01 6.01)", "-3",
				"(+ 2.174 0.321 0.405 0.2 -0.1)", "3",
                "(* 1.0 4)", "4",*/
                "(/ 16 4.0)", "4"
			};

            CheckAssertions<SchlipsNumber>(assertions);
        }
        #endregion


        #region reader makro
        [Test]
        public void Eval_10_ReaderMakroQuote_Number()
        {
            var obj = Evaluate("'10");
            AssertHelper<SchlipsNumber>(obj, 10);
        }

        [Test]
        public void Eval_10b_ReaderMakroQuote_Cons()
        {
            var obj = Evaluate("'(10)");
            Assert.IsInstanceOf<SchlipsNode>(obj);
            Assert.AreEqual("(10)", obj.ToString());
        }

        [Test]
        public void Eval_10c_ReaderMakroQuote_Lambda()
        {
            var obj = Evaluate("'(lambda () (+ 3 4))");
            Assert.IsInstanceOf<SchlipsUDF>(obj);
            var udf = obj as SchlipsUDF;

            AssertHelper<SchlipsNumber>(eval.Evaluate(udf), 7);
        }
        #endregion


        #region Define
        [Test]
        public void Eval_15_Define()
        {
            var expected = 11; var symbol = "ten";
            var result = Evaluate(String.Format("(define {0} {1})", symbol, expected));

            var actual = Evaluate(symbol);

            AssertHelper<SchlipsNumber>(actual, expected);
        }

        [Test]
        public void Eval_15b_Define_WrongParameters()
        {
            string[] falseInputs = {
                "(define a )",
                "(define a 17 19)",
                "(define 12 a)"
            };

            foreach (var input in falseInputs)
            {
                AssertException<SchlipsArgumentException>(input);
            }
        }

        #endregion define


        #region lambda
        [Test]
        public void Eval_20_Lambda_NoArguments()
        {
            var input = "((lambda () (+ 3 4)))"; var expected = 7;

            var actual = Evaluate(input);

            Assert.AreEqual(expected, actual.Value);
        }

        [Test]
        public void Eval_20b_Lambda_NoArgumentsNoFunctionBody()
        {
            var input = "((lambda () 8))"; var expected = 8;

            var actual = Evaluate(input);
            AssertHelper<SchlipsNumber>(actual, expected);
        }

        [Test]
        public void Eval_21_Lambda_OneArgumentNoFunctionBody()
        {
            var input = "((lambda (a) a) 17)"; var expected = 17;

            var actual = Evaluate(input);
            AssertHelper<SchlipsNumber>(actual, expected);
        }

        [Test]
        public void Eval_21b_Lambda_OneArgument()
        {
            var input = "((lambda (x) (+ x x)) 5)";  var expected = 10;
            
            var actual = Evaluate(input);
            AssertHelper<SchlipsNumber>(actual, expected);
        }

        [Test]
        public void Eval_22_Lambda_MultipleArguments()
        {
            var input = "((lambda (x y) (+ x y)) 3 9)"; var expected = 12;
            
            var actual = Evaluate(input);
            AssertHelper<SchlipsNumber>(actual, expected);
        }

        [Test]
        public void Eval_22b_Lambda_MultipleArgumentsNestedBody()
        {
            var input = "((lambda (x y) (+ 1 (+ 3 x) y)) 17 200)";  var expected = 221;

            var actual = Evaluate(input);
            AssertHelper<SchlipsNumber>(actual, expected);
        }

        [Test]
        public void Eval_25_Lambda_Nested()
        {
            var input = "((lambda (n) ((lambda (x) (+ x n)) 2)) 3)";  var expected = 5;
            
            var actual = Evaluate(input);
            AssertHelper<SchlipsNumber>(actual, expected);
        }

        [Test]
        public void Eval_26_Lambda_Multiple()
        {
            string[] assertions = {
               "((lambda () (+ 2 (- 3 1))))", "4",
                "((lambda () (+ 3  (-   3 1))) )", "5", //
                "((lambda (a b command) (+ a   b command)) 3 -4 (+ 1 1))", "1",
                "((lambda (d operand f) (+ d (- operand f) f)) (+ 2 (- 1 1)) -4 (+ 1 1))", "-2",
                "((lambda (n) ((lambda (x) (+ (- x -1) n)) 2)) 3)", "6"
            };

            CheckAssertions<SchlipsNumber>(assertions);
        }
        #endregion


        #region Cons
        [Test]
        public void Eval_30_Cons()
        {
            var obj = Evaluate("(cons 1 2)");
            Assert.IsInstanceOf<SchlipsNode>(obj);
            var node = obj as SchlipsNode;
            Assert.AreEqual(2, node.Count);
        }

        [Test]
        public void Eval_30_ConsList()
        {
            var obj = Evaluate("(cons 1 ())");
            Assert.IsInstanceOf<SchlipsNode>(obj);
            var node = obj as SchlipsNode;
            Assert.AreEqual(1, node.Count);
        }

        [Test]
        public void Eval_31_ConsCar()
        {
            var obj = Evaluate("(car (cons 1 2))");
            Assert.IsInstanceOf<SchlipsNumber>(obj);
            AssertHelper<SchlipsNumber>(obj, 1);
        }

        [Test]
        public void Eval_31b_ConsCdr()
        {
            var obj = Evaluate("(cdr (cons 1 2))");
            Assert.IsInstanceOf<SchlipsNumber>(obj);
            AssertHelper<SchlipsNumber>(obj, 2);
        }

        [Test]
        public void Eval_32_ConsNested()
        {
            Evaluate("(define testCons (cons 1 (cons 2 Q)))");

            var obj = Evaluate("testCons");
            var str = obj.ToString();
            Assert.AreEqual("(1 2 . Q)", str);
            AssertNode(obj, 3);

            obj = Evaluate("(car testCons)");
            AssertHelper<SchlipsNumber>(obj, 1);

            obj = Evaluate("(cdr testCons)");
            str = obj.ToString();
            Assert.AreEqual("(2 . Q)", str);
            AssertNode(obj, 2);

            obj = Evaluate("(cdr (cdr testCons))");
            AssertHelper<SchlipsSymbol>(obj, "Q");

            obj = Evaluate("(car (cdr testCons))");
            AssertHelper<SchlipsNumber>(obj, 2);
        }
        
        [Test]
        public void Eval_33_Cons_Multiple()
        {
            string[] assertions = {
                "(cons A (cons 1 (cons 2 ())))", "(A 1 2)",
                "(cons A (cons 1 '2))", "(A 1 . 2)"
            };

            CheckStringAssertions(assertions);            
        }

        [Test]
        public void Eval_34_Cons_Errors()
        {
            string[] fails = {
                "(cons A 2 3)"
            };


            foreach (var fail in fails)
            {
                Assert.Throws<SchlipsArgumentException>(() => Evaluate(fail));
            }
        }
        #endregion
        

        #region conditionals
        [Test]
        public void Eval_35_True()
        {
            var obj = Evaluate("#t");

            Assert.IsInstanceOf<SchlipsAtom>(obj);
            Assert.IsTrue(obj.IsTrue);
            Assert.IsFalse(obj.IsFalse);
        }

        [Test]
        public void Eval_35b_True_Not()
        {
            var obj = Evaluate("(not #t)");

            Assert.IsInstanceOf<SchlipsAtom>(obj);
            Assert.IsFalse(obj.IsTrue);
            Assert.IsTrue(obj.IsFalse);
        }

        [Test]
        public void Eval_36_False()
        {
            var obj = Evaluate("#f");

            Assert.IsInstanceOf<SchlipsAtom>(obj);
            Assert.IsFalse(obj.IsTrue);
            Assert.IsTrue(obj.IsFalse);
        }

        [Test]
        public void Eval_36b_False_Not()
        {
            var obj = Evaluate("(not #f)");

            Assert.IsInstanceOf<SchlipsAtom>(obj);
            Assert.IsTrue(obj.IsTrue);
            Assert.IsFalse(obj.IsFalse);
        }

        [Test]
        public void Eval_37_IfTrue()
        {
            var obj = Evaluate("(if #t 1 2)");
            AssertHelper<SchlipsNumber>(obj, 1);
        }

        [Test]
        public void Eval_37b_IfFalse()
        {
            var obj = Evaluate("(if #f 1 2)");
            AssertHelper<SchlipsNumber>(obj, 2);
        }

        [Test]
        public void Eval_37c_IfExceptions()
        {
            string[] notAllowed = {
                "(if #t 1 2 3)",
                "(if #f 1)"
            };

            Assert.Throws<SchlipsArgumentException>(() => Evaluate("(if #t 1 2 3)") );
        }

        [Test]
        public void Eval_38_And_True()
        {
            string[] shouldBeTrue = {
                "(and #t #t)",
                "(and #t)",
                "(and #t #t #t)",
                "(and (> 1 -1) #t)",
                "(and #t (= 1 1) (<= 44 121))"
            };

            SchlipsTokenObject obj;
            foreach (var maybe in shouldBeTrue)
            {
                obj = Evaluate(maybe);
                Assert.IsTrue(obj.IsTrue, maybe + " should return true");
            }
        }

        [Test]
        public void Eval_38b_And_False()
        {
            string[] shouldBeFalse = {
                "(and #t #f #t)",
                "(and #f)",
                "(and #t (= 2 2) (= 2 3) #t #f)",
                "(and #t #t (> 1 2) #t #f)",
                "(and 12)",
                "(and #t #t (+ 3 4) #t)"
            };

            SchlipsTokenObject obj;
            foreach (var maybe in shouldBeFalse)
            {
                obj = Evaluate(maybe);
                Assert.IsFalse(obj.IsTrue, maybe + " should return false");
            }
        }
        [Test]
        public void Eval_39_Or_True()
        {
            string[] shouldBeTrue = {
                "(or #f #t)",
                "(or #t)",
                "(or #f #f #t)",
                "(or (< 1 -1) (>= 17 17))",
                "(or #t (= 1 1) (<= 44 121))",
                "(or #f #f (+ 1 2) #t)",
                "(or (< 2 2) (= 2 3) #t #f)",
                "(or #f #f (> 1 2) #t #f)",
                "(or 12 #t)",
                "(or #f #f (+ 3 4) #t)"
            };

            SchlipsTokenObject obj;
            foreach (var maybe in shouldBeTrue)
            {
                obj = Evaluate(maybe);
                Assert.IsTrue(obj.IsTrue, maybe + " should return true");
            }
        }

        [Test]
        public void Eval_36b_Or_False()
        {
            string[] shouldBeFalse = {
                "(or #f #f #f)",
                "(or #f)",
                "(or #f (= 3 2) (>= 2 3) #f)",
                "(or #f (< 4 1) (> 1 2) #f (+ 2 3))",
                "(or 12)",
                "(or (+ 3 4))"
            };

            SchlipsTokenObject obj;
            foreach (var maybe in shouldBeFalse)
            {
                //viertes
                obj = Evaluate(maybe);
                Assert.IsFalse(obj.IsTrue, maybe + " should return false");
            }
        }

        [Test]
        public void Eval_40_EqualNumbers()
        {
            var obj = Evaluate("(= 1 2)");
            Assert.IsTrue(obj.IsFalse);
            Assert.IsFalse(obj.IsTrue);
        }

        [Test]
        public void Eval_40b_EqualNumbers()
        {
            var obj = Evaluate("(= 200 200)");
            Assert.IsTrue(obj.IsTrue);
            Assert.IsFalse(obj.IsFalse);
        }

        [Test]
        public void Eval_41_CompareNumbers_Success()
        {
            string[] assertions = {
                "(> 1721 18)",
                "(< 17 29)",
                "(<= 131 1311)",
                "(<= 8 8)",
                "(> 4 -2)",
                "(>= 99991 4)",
                "(>= -1 -1)",
                "(< -10 -2)"
            };
            SchlipsTokenObject obj;

            foreach (var input in assertions) 
            {
                obj = Evaluate(input);
                Assert.IsTrue(obj.IsTrue, input + " should evaluate to true");
            }
        }

        [Test]
        public void Eval_41b_CompareNumbers_Fails()
        {
            string[] assertions = {
                "(= 1721 18)",
                "(> 17 29)",
                "(<= 1311 131 )",
                "(<= 9 8)",
                "(< 4 -72)",
                "(<= 99991 4)",
                "(>= -1 1)",
                "(< -10 -12)"
            };
            SchlipsTokenObject obj;

            foreach (var input in assertions)
            {
                obj = Evaluate(input);
                Assert.IsFalse(obj.IsTrue, input + " should evaluate to false");
            }
        }

        [Test]
        public void Eval_42_CompareNumbers_Exceptions()
        {
            string[] assertions = {
                "(= 1721 Aber)",
                "(> (cons 1 2) 29)",
                "(<= 1311 131 377 )",
                "(<= #f 8)",
                "(< A4 -72)",
                "(<= 99991 #t)",
                "(>= -1)",
                "(= () -12)"
            };

            foreach (var input in assertions)
            {
                Assert.Throws<SchlipsArgumentException>(() => Evaluate(input), input + " should throw an Exception");
            }
        }

        #endregion


    }
}
