﻿
using NUnit.Framework;
using System;
using Schlips;

namespace SchlipsTest
{
    [TestFixture]
    class Test_05_Extended : SchlipsTester
    {   
        [SetUp]
        protected override void Init()
        {
            base.Init();
        }

        [Test]
        public void Extended_01_Define_ShortHand()
        {
            var obj = Evaluate("(define (minus1 n) (- n 1))");

            obj = Evaluate("(minus1 5)");
            AssertHelper<SchlipsNumber>(obj, 4);
            /*
            (define (<variable> <formals>) <body>)
             * is a shorthand for
             (define <variable>
                (lambda (<formals>) <body>)).
            */

        }

        [Test]
        public void Extended_02_DefineLambda()
        {
            Evaluate("(define add2 (lambda (q) (+ q 2)))");
            
            var expected = 3;
            var actual = Evaluate("(add2 1)");

            AssertHelper<SchlipsNumber>(actual, expected);
        }
        
        [Test]
        public void Extended_05_MakeAdder()
        {
            string[] commands = {
                "(define make-adder (lambda (three) (lambda (nineteen) (+ three nineteen))))",
                "(define add3 (make-adder 3))",
                "(add3 19)"
            };

            var result = new SchlipsTokenObject();
            var expected = 22;

            foreach (var cmd in commands)
            {
                result = Evaluate(cmd);
            }

            AssertHelper<SchlipsNumber>(result, expected);
        }

        [Test]
        public void Extended_05b_MakeAdder_MultipleAdders()
        {
            string[] commands = {
                "(define make-adder (lambda (inner) (lambda (outer) (+ outer inner))))",
                "(define add4 (make-adder 4))",
                "(add4 711)"
            };

            var result = new SchlipsTokenObject();
            var expected = 715;

            foreach (var cmd in commands)
            {
                result = Evaluate(cmd);
            }

            AssertHelper<SchlipsNumber>(result, expected);

            Evaluate("(define add5 (make-adder 5))");

            result = Evaluate("(add5 4)");
            AssertHelper<SchlipsNumber>(result, 9);

            result = Evaluate("(add4 47)");
            AssertHelper<SchlipsNumber>(result, 51);
        }

        [Test]
        public void Extended_06_MakeAdder_Shorthand()
        {

            string[] commands = {
                "(define (make-adder n) (lambda (x) (+ x n)))",
                "(define add3 (make-adder 3))",
                "(add3 71619)"
            };

            var result = new SchlipsTokenObject();
            var expected = 71622;

            foreach (var cmd in commands)
            {
                result = Evaluate(cmd);
            }

            Assert.AreEqual(expected, result.Value);
        }

        /* TODO
         * (let ((name ausdruck)..) Rumpf
         * (let 
         *   ((a (+ 1 1)) (b (+ 2 2))) 
         *   (+ a b))
         * wird zu
         * ((lambda (a b) (+ a b)) (+ 1 1) (+ 2 2))
         */
    }
}
