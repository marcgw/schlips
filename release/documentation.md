#Documentation Schlips (2014-09-21)
_Scheme, C# and Lisp_
**by: Marc Walter**  
  
A Lisp Interpreter in C#  
Target .Net Runtime: 4.5  


##Structure
###Folders
This repository contains a Visual Studio Project _LispCnet.sln_, this documentation and the three source code folders _Schlips_, _SchlipsCLI_ and _SchlipsTest_. _Schlips_ contains the interpreter library, _SchlipsCLI_ the command line interpreter, and _SchlipsTest_ contains the NUnit test cases written for the interpreter.  
The folder _release_ contains the built command line schlips-cli.exe and the schlips.dll (built for .Net 4.5). Those two files and the contents of the subfolder lisp are mandatory for execution.  

###General
My main goal was to program a solution, where Reader Macros would be easy to implement and it is possible to overwrite all definitions (also the define-function). This makes it very easy to switch between different Lisp or Scheme variants.  
Due to that the evaluation of commands comprises of three steps:  
First, a **SchlipsReader** reads strings and converts them into a tree structure of _SchlipsNode_ objects (conses or pairs in Scheme). Each _SchlipsNode_ may contain either other _SchlipsNode_ instances or _SchlipsSymbols_ or _SchlipsNumbers_.  
Then the result is passed to a **SchlipsParser** that looks up SchlipsSymbols in the root environment and returns a tree of _SchlipsNodes_, _SchlipsProcedures_, _SchlipsNumbers_ or _SchlipsSymbols_ or _SchlipsAtoms_ (e.g. Nil).  
This tree is passed to a **SchlipsEvaluator**, that returns the evaluated value.  

###Object model
Generally, all objects inherit from _SchlipsTokenObject_ that implements properties that allow the parser and evaluator to discern the objects, like ```IsTrue```, ```IsSymbol``` or ```IsFunction```. That way I can call ```possibleSymbol.IsSymbol``` instead of ```possibleSymbol is SchlipsSymbol``` which will perform a type conversion in .Net.
The inheriting classes _SchlipsAtom_, _SchlipsSymbol_, _SchlipsNumber_ override these properties.  

Functions and builtin syntax elements like ```define``` are implemented using _SchlipsProcedure_ objects, which also inherit from _SchlipsTokenObject_.  
Each _SchlipsProcedure_ contains a property ```ExpectedOperands```, that allows to test if a function was passed the correct amount of operands. Also, the property ```IsExecutable``` is used in the _SchlipsEvaluator_ to distinguish between the calls ```(lambda (n) (+ n n))```, which returns ```<Builtin:Lambda>``` and ```((lambda (n) (+ n n)) 2)```, wich returns ```4```.  

The Atoms _Nil_, _True_ and _False_ are implemented as Singletons. An empty _SchlipsNode_ also evaluates to _Nil_, but returns false for the call _IsAtomic_ because it may contain other elements.  

Both _SchlipsParser_ and _SchlipsEvaluator_ hold a reference to the root _SchlipsEnvironment_, which is created as a Singleton by the _SchlipsEnvironmentFactory_. If a new _SchlipsEnvironment_  is created, it references its parent environment (which might also be the root environment) and if a _SchlipsSymbol_ cannot be resolved in the environment, it is looked up in the parent environment.  
If it does not exist there either, nil or the _SchlipsSymbol_ might be returned, for instance _SchlipsEvaluator_ is using the functionality to return the symbol if it does not exist in the environment.  

All functionality is saved as _SchlipsProcedures_ in the _SchlipsEnvironment_. That way the language **'whacky'** (by default part of the language selection in the command line interpreter) is able to define ```confuse``` as the new define method and set ```define``` to ```nil```´. Or switch the values of ```#f``` and ```#t```.

##Thoughts
Unfortunately I did not find the  time to implement my own Garbage Collector, continuations or native compilation, yet.  
In the future, I plan to replace the _SchlipsEvaluator_ with a _SchlipsCompiler_ that will translate this tree to 'native code' for the .Net Virtual Machine, using CLR [Expression Trees](http://msdn.microsoft.com/en-US/library/bb397951.aspx), without having to change anything in both _SchlipsReader_ and _SchlipsParser_.  
The interpreter itself is compiled to a dll, that way both the command line interpreter and a GUI interpreter will be able to use it.  

##Functionality
Schlips-cli is a command line interpreter using a Read-Eval-Print-Loop (REPL) to execute user commands.  
The user may enter multiple lines of code, the commands are only evaluated if the number of opening and closing parentheses match.  

##Command Line Interpreter
If the schlips-cli is started, it loads the language scheme from the file _lisp/lang-scheme.lisp_.  
After that, the user may enter single and multi-line commands.  
If the evaluation of a command fails, an error message is shown and another command may be entered.  

###Select language
The commands ```:l``` or ```:load``` allow the user to load another language preset.  
These must be stored in this pattern ```lisp/lang-<languagename>.lisp``` and contain commands interpretable by schlips-cli.  
Supplied languages are _scheme_, _lisp_ and _whacky_.  

###Evaluate file
To evaluate a file, ```:e <filepath>``` or ```:evaluate <fielpath>``` must be used.  
Both relative and absolute file paths are allowed.  
If debug mode is activated, each line in the file is printed before evaluating it.  

###Reset interpreter
When ```:r``` or ```:reset``` are entered, all changes to the root environment are deleted and the language 'scheme' is loaded.  

###Print all possible commands
If ```:c``` or ```:commands``` are entered, all definitions in the root environment are printed.  
This is a good way to get an overview of all available commands.  

###Quit
To exit the interpreter, the following commands may be used: ```:q```, ```:quit```, ```quit```, ```exit```, ```logout```.  
  
  

##Language commands

###quote
Quote returns the literal expression it is passed and does not evaluate it.
**Operands:** One  
_shorthand for ```(quote ???)``` is ```'???```_  
**Code:**  
```
> (quote (lambda () 17))
<Builtin:Lambda>
> '(+ 1 2)
<Builtin:Add>
> (quote 1 2)
<Builtin:Quote> only expects 1 operands, but operand number 2 was passed
> '(cons 1 2)
<Builtin:Cons>
```

###lambda
A lambda evaluates to a procedure with a defined number of operands. When the procedure is actually called (by passing it the operands), the function body is evaluated in a new _SchlipsEnvironment_ with bindings for the passed operands.  
**Operands:** 2. The first must be a Cons, the second is an arbitrary SchlipsTokenObject.  
**Code:**  
```
> ((lambda () 5))
5
> (lambda (n) n)
<Builtin:Lambda>
> ((lambda (a) (+ a 1)) 2)
3
> ((lambda () (+ 1 2)) 17 22)
Arity mismatch: All operands of this Lambda are already set. Don't try to set them again.
```


###define
Save a given SchlipsTokenObject in the environment and make it retrievable using a symbol.  
**Operands:** 2. The first must be a symbol, the second is evaluated and stored in the environment.  
**Code:**  
```
> (define four 4)
Nil
> four
4
> (define plus1 (lambda (n) (+ n 1)))
Nil
> plus1
<Builtin:Lambda>
> (plus1 1)
2
```

###cons
Creates a pair (SchlipsNode) and can be used to create lists. A list is a group of conses with the Cdr being Nil.  
**Operands:** 2.  
**Code:**  
```
> (cons 4 3)
(4 . 3)
> (cons 2 3 4)
<Builtin:Cons> only expects 2 operands, but operand number 3 was passed
> (cons 1 (cons 2 3))
(1 2 . 3)
> (cons 1 (cons 2 (cons 3 ())))
(1 2 3)
```

###car
Retrieves the first Element in a List or Cons.  
**Operands:** 1  
**Code:**  
```
>
> (car (cons 0 1))
0
> (define onetwo (cons 1 2))
Nil
> (car onetwo)
1
```
###cdr
Retrieves all Elements except the first from a List or Cons.  
**Operands:** 1  
**Code:**  
```
> (define c123 (cons 1 (cons 2 (cons 3 ()))))
Nil
> (cdr c123)
(2 3)
> (car (cdr c123))
2
> (cdr (cdr c123))
(3)
> (cdr (cdr (cdr c123)))
()
```

###if
If the first operand evaluates to true, the second one is evaluated. If not, the third one is.  
**Operands:** 3.  
**Code:**  
```
> (if #t 1 2)
1
> (if #t (+ 1 2) (- 2 4))
3
> (if #f (+ 1 2) (- 2 4))
-2
```

###not
If the first operand evaluates to true, false is returned and vice versa.  
**Operands:** 1.  
**Code:**  
```
> #t
True
> (not #t)
False
> (not (> 11 -11))
False
```

##and
Evaluates to true if all passed operands evaluate to true.  
**Operands:** At least 1.  
**Code:**  
```
> (and #t (> 1 2))
False
> (and #t (> 11 2) (= 2 (+ 1 1)))
True
```

##or
Evaluates to true if one of the passed operands evaluates to true.  
**Operands:** At least 1.  
**Code:**  
```
> (or #f #f (> 1 2) (>= 12 12))
True
> (or #f)
False
```

##Arithmetic operations
###add
Evaluates the sum over all passed numbers.  
**Operands:** At least 1 number.  
**Code:**  
```
> (+ 1 -1 8 -1 2)
9
> (+ 1)
1
> (+ 99 -101)
-2
```

###subtract
Evaluates the difference over all passed numbers.  
**Operands:** At least 1 number.  
**Code:**  
```
> (- 1)
-1
> (- -4 1)
-5
> (- 17 33 140903023)
-140903039
```

###comparison
Arithmetic comparisons evaluate to true or false.  
**Operands:** 2 numbers.  
Implemented are:  

*	Equal **=**
*	Greater than **>**
*	Greater equal **>=**
*	Smaller than **>**
*	Smaller equal **>=**

**Code:**  
```
> (< 1 2)
True
> (>= 1 1)
True
> (> 17 19)
False
> (>= 17 -19)
True
> (= 3 2)
False
```
